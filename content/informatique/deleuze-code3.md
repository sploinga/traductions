Title: Deleuze pour les développeurs : Est-ce que l'espace lisse de l'Open Source nous sauvera ?
Date: 2013-04-11 20:20
Tags: libriste, philosophie, code
Summary: Si vous voulez vraiment comprendre la technologie actuelle, vous devriez vous familariser avec la philosophie de Gilles Deleuze. Malheureusement pour les technophiles, elle est ancrée dans une tradition et un style d’écriture probablement opaque pour eux. Dans cette série de billets, l’auteur essaie d’expliquer Deleuze dans des termes compréhensibles par des programmeurs. Voici le troisième et dernier de la série.
Author: Steve Klabnik
Source: <a href="http://words.steveklabnik.com/deleuze-for-developers-will-smooth-spaceopen-source-suffice-to-save-us"><i class="icon…external…link"></i> Deleuze for Developers: will {Smooth space,Open Source} suffice to save us?</a>

Deleuze et Guattari emploient souvent la notion d'espace lisse dans *Milles plateaux*. L'autre jour, je suis tombé sur cette citation :

>ne pensez jamais qu'un espace lisse arrivera à tous nous sauver

J'ai aussi récemment remarqué que la notion d'[hégémonie culturelle](http://fr.wikipedia.org/wiki/H%C3%A9g%C3%A9monie_culturelle) s'applique à la monoculture Google que l'on peut observer sur Internet. L'idée d'hégémonie culturelle est que vous ne dominez pas que par la loi ou l'argent, mais aussi par la culture. L'adoption progressive du protocole SPDY de Google est un bon exemple. Cette expansion culturelle va tout à fait de pair avec l'existence de standards ouverts.

Il y a donc cet lien entre l'espace lisse et l'open source, et la bataille des nomades avec l'appareil d'État. Nous nous intéressons aux seconds dans un moment, mais pour l'instant, définissons d'abord la notion d'espace lisse.

La métaphore spatiale
-----------------------------

La première chose à comprendre est que Deleuze et Guattari sont des grands fans de métaphores spatiales. La plupart des problèmes peuvent se projeter dans des schémas pluridimensionnels. C'est Manuel de Landa qui m'a introduit à cette technique, à travers son excellente leçon « Deleuze and the use of the
genetic algorithm in architecture », disponible en version [vidéo](https://www.youtube.com/watch?v=50-d_J0hKz0) ou [texte](http://www.cddc.vt.edu/host/delanda/pages/algorithm.htm). Pour résumer quelques éléments, parlons du [problème aux n dames](http://fr.wikipedia.org/wiki/Probl%C3%A8me_des_huit_dames). Nous avons un plan avec un seul espace et plaçons une dame dessus:

	.-.
	|♕|
	.-.

Cette dame ne menace personne, puisqu'elle est seule. Il y a donc une solution évidente au problème quand il y a une seule dame. Quand l'échiquier n'a que deux ou trois rangées, il n'y a aucune solution: 

	.---.
	|♕| |
	-----
	| | |
	.---.


Où que vous placiez une autre dame, elle sera toujours menacée par la précédente. C'est assez simple à déterminer en faisant toutes les cases possibles à la main. Mais vous aurez déjà de nombreuses cases et combinaisons possibles pour un échiquier assez simple. Pour un vrai échiquier, il y a 4 426 165 368 placements possibles et seulement 92 solutions. Une épingle dans une botte de foin ! Et notre algorithme précédent (tenter toutes les positions) n'est pas très efficace.

Une manière de faire est de prendre l'état actuel de notre échiquier et de lui donner un score. Si, lorsque nous plaçons une nouvelle dame, notre score s'améliore, on garde la disposition, sinon on la jette. Ça donne une courbe qui a cette allure si on place les scores sur l'axe y et les placements de dame sur l'axe x. 

	                      .dAHAd.
	                    .adAHHHAbn.
	      .dAHAd.     .adAHHHHHHAbn.
	    .adAHHHAbn.  .adAHHHHHHHHAbn.
	   dHHHHHHHHHHHHHHHHHHHHHHHHHHHHHb
	  dHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHb

Ce qu'on veut faire, c'est donc continuer à garder les positions satisfaisantes jusqu'à arriver en haut de la courbe. Mais le problème est que cette courbe peut avoir une multitude de maxima. Et peut être en plusieurs dimensions. Générer la courbe entière et la parcourir pour en trouver les maximums semble computationnellement excessif. Comment est-ce qu'on fait alors ? On utilise les [algorithmes génétiques](http://fr.wikipedia.org/wiki/Algorithme_g%C3%A9n%C3%A9tique) ! 

L'un d'entre eux est appelé le [recuit simulé](http://fr.wikipedia.org/wiki/Recuit_simul%C3%A9). De même qu'en métallurgie on peut optimiser la résistance des métaux en contrôlant la vitesse de refroidissement, en mathématique on peut contrôler la vitesse à laquelle on accepte de redescendre une pente, de sorte qu'au fur et à mesure du temps, on redescende de moins en moins vite les espaces intéressants, pour arriver le plus vite possible aux maxima/minima.

Discret versus Continu
-----------------------------

>Si nous savons que l'ennemi est prêt à attaquer et que nos hommes le sont également, mais que nous ne savons pas que l'état du sol rend le combat presque impossible, nous ne sommes qu'à moitié sur le chemin de la victoire.

>Sun Tzu, *L'art de la guerre*

Une autre caractéristique de cette projection particulière est qu'elle transforme notre problème discret en un problème continu. Une tactique très utile quand vous perdez est de changer de champ de bataille. Notre algorithme génétique a besoin d'un espace continu, mais notre problème est que notre échiquier est discret. 

Noter les positions nous permet de transformer le problème. Nous passons d'un échiquier discret à des nombres réels avec lesquels on peut utiliser notre algorithme. 

Les espaces lisses et striés
-----------------------------

>Les tactiques militaires sont semblables à l'eau. Pour l'eau la course naturelle est de partir de haut pour ruisseler jusqu'en bas. De même, il faut éviter ce qui est fort et frapper ce qui est faible. Comme l'eau, prendre la route de moindre résistance. L'eau trace sa route en s'adaptant au relief. Le soldat gagne en fonction de la nature de son ennemi. C'est pourquoi l'eau comme la guerre n'ont aucune forme fixe.

>Sun Tzu, *L'art de la guerre*

Ok. Maintenant nous pouvons parler d'espace lisse et d'espace strié. Deleuze et Guattari ont plusieurs noms pour ce concept, mais pour nous le plus simple sera de parler d'[espace riemannien](https://fr.wikipedia.org/wiki/G%C3%A9om%C3%A9trie_riemannienne) et d'[espace euclidien](https://fr.wikipedia.org/wiki/G%C3%A9om%C3%A9trie_euclidienne). Lisse &rarr; Riemann, strié &rarr; Euclide. 

L'espace euclidien est utilisé comme modèle canonique dans la plupart des mathématiques. C'est une géométrie des lignes. Riemann au contraire décrivait des courbes. Einstein utilisa la géométrie riemanniene pour sa théorie de la relativité. 

Pensez-y de cette manière: vous êtes dans une voiture, en ligne droite. Vous continuez pendant des dizaines de kilomètres, sans virage ou routes incidentes. Vous êtes dans un espace lisse: vous vous déplacez facilement d'un point à l'autre. Maintenant, si quelqu'un vous pose des panneaux stop tous les kilomètres, vous êtes dans un espace strié.

Pour D&G, l'appareil d'État tente sans cesse de s'approprier les courbes lisses pour les transformer en lignes droites et étalonnées, souvent avec des visées impérialistes, en imposant ses normes via la quantification. Prenez la mer par exemple: si vous y mettez longitude et latitude, vous pouvez vous y repérer et naviguer pour la conquérir. 

Le lisse est aussi un territoire que les start-ups passent leur temps à essayer de conquérir. Prenez l'amitié par exemple. C'était une non-valeur, une quantité fluide. Puis le «j'ai beaucoup d'amis» est devenu «j'ai 200 amis et tu en as 100». Cette quantification permet la marchandisation: Facebook vend de la pub. C'est aussi pourquoi la plupart des start-ups n'ont pas de modèle économique au début. Elles ont besoin de strier l'espace avant de pouvoir le vendre. Parfois vous tapez dans la poussière et vous trouvez de l'or, mais souvent vous avez besoin de raffiner la matière première pour la vendre. 

Les nomades sont des entités qui naviguent et vivent dans les espaces lisses. 

Comment créer un espace lisse ?
-----------------------------

Vous pouvez vous souvenir de la [somme de Riemann](https://fr.wikipedia.org/wiki/Somme_de_Riemann). Son génie est de d'abord strier l'espace avant de le relisser. Vous commencez par une courbe, et ensuite vous approximez l'aire en dessous en la divisant en stries. Ce jeu d'aller et retour entre les espaces lisses et striés arrive souvent, et aucun espace n'est complètement lisse ou complètement strié. D&G soutenait que la gauche devait s'attacher à créer le plus possible d'espace lisse puisque le capitalisme et l'État passent leurs temps à essayer de tout strier. Ceci dit, si l'espace libre est nécessaire, il n'est pas suffisant. Le capitalisme arrive à naviguer tant que mal faire se peut dans les espaces lisses. Je cite un passage un peu long de *Milles Plateaux* (Chapitre «Le lisse et le strié», sous-chapitre sur le modèle physique):

>Non seulement l'usager comme
tel tend à devenir un employé, mais le capitalisme opère moins
sur une quantité de travail que sur un processus qualitatif com­plexe qui met en jeu les modes de transport, les modèles urbains,
les médias, l'industrie des loisirs, les manières de percevoir et
de sentir, toutes les sémiotiques. C'est comme si, à l'issue du
striage que le capitalisme a su porter à un point de perfection
inégalé, le capital circulant recréait, nécessairement, reconstituait
une sorte d'espace lisse où se rejoue le destin des hommes. Certes,
le striage subsiste sous ses formes les plus parfaites et sévères (il
n'est plus seulement vertical, mais opère en tout sens); toutefois, il renvoie surtout au pôle étatique du capitalisme, c'est-à­-dire au rôle des appareils d'Etat modernes dans l'organisation du
capital. En revanche, au niveau complémentaire et dominant d'un
capitalisme mondial intégré (ou plutôt intégrant), un nouvel espace
lisse est produit où le capital atteint à sa vitesse « absolue »,
fondée sur des composantes machiniques, et non plus sur la
composante humaine du travail. Les multinationales fabriquent
une sorte d'espace lisse déterritorialisé où les points d'occupation
comme les pôles d'échange deviennent très indépendants des
voies classiques de striage. Le nouveau, c'est toujours les nou­velles formes de rotation . Les formes actuelles accélérées de la
circulation du capital rendent de plus en plus relatives les dis­tinctions du capital constant et variable, et même du capital fixe
et circulant; l'essentiel est plutôt la distinction d'un capital strié
et d'un capital lisse, et la manière dont le premier suscite le
second, à travers des complexes qui survolent les territoires et les
Etats, et même les types différents d'Etats.

La première partie à propos des usagers est vraiment à mettre en parallèle avec le dicton [«Si vous ne payez pas pour le produit, c'est que vous êtes le produit»](http://fichiers.sploing.fr/404533832.jpg). À propos de rapidité du capital, pensez au [trading à haute-fréquence](http://fichiers.sploing.fr/6a00d8341d3df553ef013487d72225970c-pi). Et sinon, repensez à McDonald qui n'est que partiellement affecté par les lois américaines. Les multinationales opèrent en dehors des États pour partie

C'est pourquoi être simplement lisse ne suffira pas à nous sauver.
