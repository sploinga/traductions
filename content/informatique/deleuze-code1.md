Title: Deleuze pour les développeurs : mixtes et agencements
Date: 2012-12-10 10:20
Tags: libriste, philosophie, code
Traducteur: Nicolas Karageuzian
Summary: Si vous voulez vraiment comprendre la technologie actuelle, vous devriez vous familariser avec la philosophie de Gilles Deleuze. Malheureusement pour les technophiles, elle est ancrée dans une tradition et un style d’écriture probablement opaque pour eux. Dans cette série de billets, l’auteur essaie d’expliquer Deleuze dans des termes compréhensibles par des programmeurs. Voici le premier de la série.
Author: Steve Klabnik
Source: <a href="http://words.steveklabnik.com/deleuze-for-developers-assemblages/"><i class="icon-external-link"></i> Deleuze for Developers: Assemblages</a>

Des philosophes grecs, comme Démocrite, soutenaient qu’il existait des ἄτομος. Des « insécables ». « ἄ »  signifiant « ne peut » et « τέμνω » « couper ».

C’était la plus petite chose existante. Vous ne pouviez les couper en deux. Tout était fait entièrement d’atomes.

Et devinez quoi ? La science moderne a trouvé des choses plus petites que les atomes tels que Démocrite les concevait.

Sacrebleu ! En tout cas, ce qui est sûr, c’est que certaines choses sont constituées d’autres choses, plus petites. Ça c’est certain. Nous ne savons toujours pas quels sont les composants élémentaires, mais parler de mixtes est un pari assez sûr. Même si les atomes sont constitués de plus petits éléments, nous savons qu’en tout cas ils forment des molécules et des corps plus grands. Ils sont entre deux, mixtes.

Alors comment peut-on parler de mixtes ? Un des outils fondamentaux du programmeur, c’est l’abstraction. Donc on peut décider q’un mixte est une boîte noire, et le traiter comme une entité, sans se soucier de ce qu’on a mis dedans.

C’est bien, c’est pratique mais il y a un problème : quand on commence à penser à une collection de choses comme juste une chose, on manque alors beaucoup de détails importants. Comme nous le savons, toutes les abstractions sont faillibles, alors en quoi une boîte noire est-elle faillible ?

Pensez à votre corps : c’est un dispositif complexe. Il est fait d’organes et d’une peau qui les enveloppe. Le tout est une boîte noire si on ne l’ouvre pas. Si nous avions vécu il y a quelques centaines d’années, parler de boîte noire pour le corps aurait vraiment été approprié. Après tout, si vous enlevez son cœur à quelqu’un, il meurt, sans que vous sachiez exactement pourquoi. La boîte a besoin de toutes ses parties. Aujourd’hui, les choses ont un peu changé. On peut transplanter un cœur, et il existe même des cœurs artificiels. Quelqu’un avec un cœur artificiel n’est pas exactement la même personne qu’avant, mais ça reste une personne. Penser un corps comme un tout n’est plus aujourd’hui une abstraction pertinente.

Éloignons-nous  de la biologie et considérons à présent les applications web. Voici le diagramme d’architecture d’un service web typique:

![Agencement d’un serveur web typique](|filename|images/steveklabnik_24382217466576_raw.png)

Ça a l’air normal, non ? Modifions-le un peu.

![Agencement de ce même serveur, légèrement modifié](|filename|images/steveklabnik_24382219810338_raw.png)

C’est le même service, n’est-ce pas ? Nous avons introduit quelques esclaves de base de données et un reverse proxy, qui nous permet d’enlever un des serveurs d’applis. D’un certain point de vue, ce service est équivalent au premier ; d’un autre, c’est différent : Il y a un composant supplémentaire, et ce composant crée plus de complexité. Nous aurons peut-être besoin de recruter un DBA et quelqu’un de talentueux pour blinder les confs. En d’autres termes, de nouvelles compétences sont nécessaires.

Alors comment appelle-t-on cette chose ? Un agencement. C’est constitué de plus petits agencements ; après tout, notre application peut utiliser le pattern Reactor, le modèle du worker Unicorn ou le modèle de threads de la JVM.

Selon le philosophe [Levi Bryant](http://larvalsubjects.wordpress.com/2009/10/08/deleuze-on-assemblages/) : 
>« Les agencements sont composés d’éléments ou d’objets hétérogènes qui entrent en relation les uns avec les autres »

Dans la métaphore du corps, ce sont les organes, et dans le diagramme système, ce sont les serveurs d’application, bases de données et files d’attente de tâches. Les agencements sont aussi des «objets» dans ce contexte, donc vous pouvez penser n’importe lequel de ces objets comme étant fait d’autres objets. Mais penser que tous ces objets forment un tout, comme une boîte noire, est pour nous incorrect, ou au mieux une abstraction erronée. Ces mixtes sont fait d’autres éléments que nous savons décomposer.

Regardons cet autre excellent exemple d’agencement : Internet. Les ordinateurs s’y connectent, déconnectent, reconnectent. Ils peuvent apparaître par intermittence ou bien rester là pour longtemps. Internet reste Internet même quand votre ordinateur est éteint. Mais nous en parlons comme d’un tout. C’est ce que Deleuze désigne par le terme d’agencement.

Il y a une bonne raison pour l’utilisation du terme générique d’« objet » ou « élément » par Bryant quand vous parlez d’agencement : les objets peuvent être des gens, des idées ou des parties corporelles. C’est là où l’idée informatique de système se distingue de celle d’agencement : vous pouvez intégrer vos utilisateurs dans la notion flexible d’agencement, c’est plus difficile quand vous parlez d’un système.

Et alors ? Et bien je voulais introduire ces termes élémentaires de mixtes et d’agencements et cette manière de raisonner. Ce sont des patrons de raisonnement. Des diagrammes. Dans les billets à venir, je vais utiliser ce patron de raisonnement pour m’intéresser à des problèmes particuliers, mais il vous fallait d’abord comprendre les termes de base.
