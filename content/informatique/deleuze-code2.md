Title: Deleuze pour les développeurs : déterritorialisation
Date: 2013-04-09 20:20
Tags: libriste, philosophie, code
Summary: Si vous voulez vraiment comprendre la technologie actuelle, vous devriez vous familariser avec la philosophie de Gilles Deleuze. Malheureusement pour les technophiles, elle est ancrée dans une tradition et un style d’écriture probablement opaque pour eux. Dans cette série de billets, l’auteur essaie d’expliquer Deleuze dans des termes compréhensibles par des programmeurs. Voici le deuxième de la série.
Author: Steve Klabnik
Source: <a href="http://words.steveklabnik.com/deleuze-for-developers-assemblages/"><i class="icon-external-link"></i> Deleuze for Developers: Assemblages</a>

Réexaminons le diagramme de cet agencement:

![What's up with this line?](|filename|images/steveklabnik_24388447125816_raw.png)

Qu'est-ce que cette ligne a de particulier ?

Et bien, ce n'est pas parce que les agencements sont relativement amorphes qu'il n'est pas du tout possible de différencier ce qui en fait partie ou non. Cet agencement particulier délimité un territoire: tout ce qui est dans la ligne fait partie du territoire, et le reste est dehors. Un peu comme pour un château: 

![Fort Pitt, mode Vauban](|filename|images/steveklabnik_24388459420680_raw.jpg)

Ou, puisque nous ne parlons pas que d'espace physique, un groupe ou un cercle social:

![Core team de Ruby on Rails](|filename|images/steveklabnik_24388506430020_raw.png)

Bien sûr, le monde s'étend largement outre le cœur de l'équipe. Mais la ligne rose désigne les frontières de l'agencement qui consiste ce cœur d'équipe.

Déterritorialisation
-----------------------------

Donc qu'est-ce qui arrive à ces frontières lorsqu'on les traverse ? Et bien, votre château est envahi. Un nouveau membre entre dans l'équipe. De nouveaux serveurs sont mis en ligne. Ce processus est appelé déterritorialisation. Réparer les lignes cassés, reformer une frontière, ça s'appelle la reterritorialisation. Je suis récemment tombé sur un symbole de déterritorialisation intéressant: le logo Open Source.

![Logo Open Source](|filename|images/steveklabnik_24388521022692_raw.gif)

Visuellement, ce logo communique l'idée de déterritorialisation que contient le geste d'ouverture de votre code: ce qui est interne est exposé dehors. Les murs ont été cassés !

Un autre exemple: en construisant un service web, on commence juste avec notre service:

![Schéma d'un service web](|filename|images/steveklabnik_24388526532744_raw.png)

Nous avons ici un serveur d'application, une base de données et les données d'identification des utilisateurs. Ces composants forment notre agencement. Souvenez-vous que les concepts abstraits sont des objets, tout comme notre code et nos serveurs ! Nos identifiants sont stockés dans la base de données, de sorte que personne ne sort du service. 

À présent, nous décidons de permettre aux gens de s'identifier avec Twitter:

![Service web+Twitter](|filename|images/steveklabnik_24388533077814_raw.png)

Twitter devient le moyen d'identification le plus courant sur notre site. À présent, l'agencement de Twitter a déterritorialisé le nôtre: 

![Service web déterritorialisé+Twitter](|filename|images/steveklabnik_24388535026314_raw.png)

Nous pouvons minimiser les effets, autrement dit reterritorialiser notre service, en essayant de privilégier notre système interne d'authentification, et en ne faisant que connecter celui de Twitter au nôtre. 

![Service web reterritorialisé+Twitter](|filename|images/steveklabnik_24388627455810_raw.png)

À présent, en intégrant complètement Twitter dans notre service, il fait partie de notre assemblage. Bien sûr, de nombreuses interconnexions ont été négligées ici. Mais de notre point de vue, elles ne nous affectent pas. En revanche, les décisions de Twitter sont à présent essentielles pour la bonne marche de notre service. Leurs mises à jours et changement de politique nous touchent directement. 

Il y a aussi une re-déterritorialisation plus subtile et secondaire: notre code et notre service étaient avant isomorphiques. Mais à présent notre code a réclamé la possession d'un nouveau territoire, et l'ancien code n'est plus qu'un sous-agencement de l'agencement total. 

Un exemple avec Git
-----------------------------

La notion précédente de déterritorialisation reposait largement sur la notion de dépendance, mais ce n'est pas nécessairement ainsi. Prenons un autre exemple:

Toute requête d'intégration Github est un acte de déterritorialisation, et toute intégration est une de réterritorialisation. Considérez ce petit dépôt, avec trois commits:

![Petit dépôt Github](|filename|images/steveklabnik_24388550633502_raw.png)

Vous le clonez:

![Petit dépôt cloné](|filename|images/steveklabnik_24388551164862_raw.png)

Vous ajoutez un commit, et un nouvel objet dans votre dépôt: 

![Petit dépôt agrandi](|filename|images/steveklabnik_24388553101194_raw.png)

Puis vous m'envoyez un mail, en me demandant de copier votre dépôt:

![Demande de fusion](|filename|images/steveklabnik_24388554578454_raw.png)

J'aime le code, donc je fais un `git fetch`:

![Fusion récupérée](|filename|images/steveklabnik_24388557943410_raw.png)

Déterritorialisé !

À présent, la copie locale: 

![Fusion copie](|filename|images/steveklabnik_24388560627354_raw.png)

et votre dépôt s'est reterritorialisé. Ces étapes ont lieu tellement vite que vous ne vous en rendez probablement pas compte. Mais conceptuellement, c'est ce qui arrive.

Un exemple social
-----------------------------

Un dernier exemple qui est encore moins lié au code: entrer dans un nouveau groupe social. Vous êtes à une conférence, et il y a quatre personnes qui discutent: 

![Quatre personnes discutent de Ruby](|filename|images/steveklabnik_24388564936014_raw.png)

Ils forment un agencement que l'on appelle une conversation. Elle parle des nouvelles amélioration de Ruby 2.0, ce qui veut dire que ça va être particulièrement intéressant. Hé. Et vous décidez que votre opinion a de l'importance, donc vous déterritorialisez l'agencement, et à supposer que l'on ne vous ignore pas, vous le reterritorialiser autour de vous. 

![Entrée dans le groupe des fans de Ruby](|filename|images/steveklabnik_24388566884064_raw.png)

La langue que nous utilisons est proche du vocabulaire deleuzien: nous entrons ou nous nous intégrons dans une conversation. 

Conclusion: Les diagrammes
-----------------------------

Nous pouvons appeler ces dessins «diagrammes» ou «machines abstraites». Ils peuvent représenter ces types de relations conceptuelles de manière visuelle, ce que je trouve très pratique pour la compréhension. Les développeurs appellent ces machines abstraites «design patterns» en anglais, ou patron de conception en français. 

À présent que vous avez vu le processus que les agencements utilisent pour entrer en rapport, j'espère que vous les trouvez utiles. Parce que leur niveau d'abstraction permet de les réutiliser dans d'innombrables situations.
