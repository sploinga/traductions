Title: La compassion des salons mondains
Date: 2012-11-17 20:00:15
Summary: Travailler quelques années au Parlement Européen vous change à jamais. Ma manière de voir les choses a changé. J’imagine qu’il vous arrive la même chose quand vous passez de l’autre côté du miroir.
Source: <a href="http://theembeddedcitizen.blogspot.be/2012/11/in-twighlight-zone-of-compassion.html"><i class="icon-external-link"></i> In the Twighlight Zone of Compassion</a>
Author: Henrik Alexandersson

Travailler quelques années au Parlement Européen vous change à jamais[^1]. Ma manière de voir les choses a changé. J’imagine qu’il vous arrive la même chose quand vous passez de l’autre côté du miroir.

Certaines choses sont tout bêtement absurdes. Comme la soirée de solidarité avec les victimes du tremblement de terre à Haïti. Ou le déjeuner à la française avec trois plats pour discuter de l’obésité en Europe. Ou le buffet avec champagne et cocktails qui ouvrait une exposition contre la conduite en état d’alcoolémie.

D’autres choses sont vraiment irréalistes. À commencer par une récente réception mondaine en faveur des sans-abris.

Les eurodéputés peuvent accueillir les expositions qu’ils veulent dans les locaux du Parlement. C’est souvent utilisé par des boîtes privées pour montrer directement leur travail aux députés.

De temps à autre, ces expositions sont organisées par des associations caricatives. Cette fois-ci, c’était pour promouvoir une association qui aide les *sans-abris*. Fort bien. Mais quelle drôle d’organisation…

Pour commencer, quelques dix à vingt sculptures de sans-abris grandeur nature en métal ont été placées dans la zone d’exposition à côté de posters expliquant les problématiques.

Ensuite, l’exposition fut ouverte à coup de discours interminables par un eurodéputé et un porte-parole de l’association. Puis, champagne !

*Je n’oublierai jamais ce moment. Des costumes et des robes hors de prix, des coiffures et des parfums français, les invités buvant du champagne en mangeant des petits fours, et ce au milieu de statues statiques représentant des citoyens bien moins chanceux.*

Cette soirée n’a même pas servi à lever des fonds. Concrètement, rien n’a été fait pour ou donné à des sans-abris. Tout l’intérêt de l’exercice était de montrer l’implication et la compassion des invités, et ce avec une très grande hauteur de vue.

Le problème pour moi est que ce type de comportement ne paraît pas étrange à l’eurodéputé moyen. Il ne trouve pas cela élitiste, prétentieux, choquant ou même bizarre. C’est son quotidien dans sa bulle politique.

Un eurodéputé normal n’est jamais plus proche d’un sans-abri que lorsqu’il file à toute allure à l’arrière de sa Mercedes noire du Parlement.

*Voilà à quoi ressemble la classe politique dominante. La bande des négligés de la morale, qu’ils soient de droite ou de gauche, du nord ou du sud. La vue des Very Important People dans leur tour d’ivoire.*

Moi et mon patron, le pirate suédois Christian Engström, avons regardé sans trop y croire cette scène extravagante avant de sortir du Parlement, pour nous payer une pinte ou deux dans le monde réel, dehors.

Sur la place du Luxembourg, nous avons trouvé un vrai sans-abri belge. Nous avons envisagé de le ramener au Parlement à la réception. Mais nous nous sommes dit que ce serait déplacé…


[^1]: Henrik Alexandersson est assistant de l’eurodéputé pirate Christian Engström au Parlement européen. 

***
<sub>Vous pouvez aussi lire la [version suédoise](http://henrikalexandersson.blogspot.be/2012/11/i-medkanslans-skymningsland.html) du texte.</sub>
