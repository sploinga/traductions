Title: Les logiciels sont-ils brevetables ? Un professeur de droit vous répond.
Date: 2012-03-24 10:00:15
Summary: Interprétation de l’article 52 de la Convention sur le Brevet Européen. Le Docteur Karl Friedrich Lenz parvient à la conclusion que les chambres de recours spécialisées de l'Office européen des brevets ont régulièrement accordé des brevets pour des programmes, quand ceux-ci sont des programmes de traitement de données, en tant que tels.
Author: Karl Friedrich Lenz
Source: <a href="http://eupat.ffii.org/analysis/epc52/exeg/index.de.html"><i class="icon-external-link"></i> Auslegung von Artikel 52 des Europäischen Patentübereinkommens hinsichtlich der Frage, inwieweit Software patentierbar ist.</a>

>Le Docteur Karl Friedrich Lenz, Professeur de Droit allemand et européen à l'université Aoyama Gakuin à Tokyo, étudie, avec les méthodes juridiques d'interprétation généralement acceptées, quelle signification doit être attribuée à l'article 52 de la CEB, et parvient à la conclusion que les chambres de recours spécialisées de l'Office européen des brevets ont, depuis quelque temps, régulièrement accordé des brevets pour des programmes, quand ceux-ci sont des programmes de traitement de données, en tant que tels. Ces chambres montrent aussi une propension préoccupante à faire prévaloir leurs positions propres sur celles du législateur.

1.    [Remarque préliminaire](#remarque)
*    [Texte de la convention](#texte)
*    [Interprétation systématique](#systématique)
*    [Interprétation téléologique](#téléologique)
*    [Interprétation historique](#historique)
*    [Interprétation conforme à la constitution](#constitution)
*    [Résultat intermédiaire](#intermédiaire)
*    [Discussion des pratiques de l'Office européen des brevets](#pratiques)
*    [Lectures complémentaires (recommandations de l'éditeur)](#complémentaires)

1. Remarque préliminaire    {#remarque}
-----------------------------
L'auteur a une idée précise de la pertinence ou non en droit politique de la brevetabilité des logiciels. Mais ce n'est pas de cela qu'il s'agit ici. Il s'agit plutôt d'établir, en ne recourant qu'à des méthodes juridiques d'interprétation généralement acceptées la signification qui doit être attribuée au texte actuellement en vigueur.

La jurisprudence actuelle est aussi discutée rapidement à la fin. Ici il s'agit avant tout de déterminer le contenu du traité, non pas de présenter des jugements.

2. Texte de la convention    {#texte}
-----------------------------
Le texte est le point de départ de toute interprétation. Le paragraphe 2 établit que: Des programmes pour des systèmes de traitement des données (NdT: ie des ordinateurs) ne sont pas considérés comme des inventions. Le paragraphe 3 restreint ce que le paragraphe 2 oppose à la brevetabilité, en ce que le brevet ne concerne que les objets ou activités cités en tant que tels.

C'est ce § 3 qu'il faut interpréter.

D'abord, le paragraphe 3 ne dit pas explicitement que seul le logiciel, en tant que tel, n'est pas brevetable. Il dit plutôt que seuls les « objets et activités » cités ne le sont pas. Le logiciel entre dans cette catégorie, mais il faut néanmoins s'en tenir au fait qu'il s'agit d'une restriction d'ensemble pour tous les cas cités, qui n'est pas conçue seulement pour le logiciel. Au paragraphe 2, quinze catégories de cas en tout sont citées. Il est tout à fait possible que les mots « en tant que tel » n'aient pas tous la même portée juridique dans tous ces cas.

On peut se permettre ici une critique envers l'auteur de l'article 52. La restriction à quinze catégories absolument différentes de cas au paragraphe 2, conjuguée à l'utilisation d'une expression unique «en tant que tel», conduit presque fatalement à l'inadéquation de cette expression à la plupart des catégories. La raison de la difficulté à comprendre le concept « logiciel en tant que tel » vient probablement de là. Si l'auteur du texte avait trouvé une formulation conçue à dessein pour le domaine du logiciel et valable dans ce seul cas, cela serait probablement beaucoup plus simple à comprendre. Cette restriction uniforme, valable pour toutes les catégories, a peu de chances d'être très compréhensible et applicable de la même manière à toutes.

Ensuite il découle de la formule « s'oppose seulement dans la mesure où », que le paragraphe 3 oblige à une limitation des effets du paragraphe 2, limitation qui est en partie nécessaire. De là, ne peut être compatible toute interprétation qui, unilatéralement, soit conduit à l'absence de toute restriction, soit conduit à une exclusion totale d'une des catégories de cas du paragraphe 2.

Maintenant, on cherche à savoir ce qui doit être compris par « logiciel en tant que tel », et surtout ce qu'est le contraire de ce concept.

Pour se préparer à cette question, essayons d'abord de voir si les programmes sont pour des systèmes de traitement de données des « objets » ou « activités », au sens du paragraphe 3.

Le programme repose sur une activité, sans en être une à proprement parler, donc est un objet.

Cet objet peut prendre plusieurs formes. Un programme est d'abord écrit dans une langue compréhensible par un humain. Ce texte original est ensuite traduit sous une forme exécutable par un ordinateur. Les deux formes peuvent être enregistrées sur un support de données (par exemple un CD) et peuvent être imprimées sur papier. Une forme exécutable peut en plus être justement exécutée.

Dans l'usage de la langue il est impossible de trancher laquelle de ces différentes formes peut être comprise comme « logiciel en tant que tel », et de déterminer si un autre concept vaut pour les autres formes. Celui qui soutient le contraire doit prouver de quel autre concept il s'agit, et quelle forme de l'objet logiciel est ainsi visée.

Une autre possibilité serait de trancher à partir de l'interaction du logiciel avec d'autres objets. Ainsi le logiciel en tant quel tel serait le domaine où le logiciel n'interagit pas ou pas suffisamment avec d'autres objets.

Alors on pourrait établir la théorie qu'il est possible de distinguer entre le logiciel en tant que tel, et le logiciel qui est bel et bien exécuté par un ordinateur. Car dans ce cas le logiciel interagit conformément à sa nature avec l'ordinateur.

Cette conception est à vrai dire dure à concilier avec les termes du paragraphe 3. Puisque chaque logiciel est destiné à tourner sur un ordinateur, la compréhension sus-citée ne clôt rien et conduit de plus l'exception au paragraphe 2 à complètement tourner à vide. Comme toujours ce qui nous limite est qu'une considération complètement unilatérale est en contradiction avec la formule « seulement dans la mesure où ».

Il reste la possibilité d'exiger une action sur d'autres objets qui dépassant la simple exécution par un ordinateur, . Si on choisit cela, la question se pose cependant immédiatement de la portée de cette action. Est-ce que l'action sur un périphérique à l'ordinateur, comme un écran, suffit ? Rien dans la formulation ne permet de le conclure. C'est pourquoi je ne tiens pas pour obligatoire de comprendre de la sorte « un logiciel en tant que tel ».

Aucun tableau très clair de la situation ne découle du texte même. Cela doit être gardé comme résultat intermédiaire de l'interprétation globale. La seule chose certaine est que la formulation unique de l'alinéa 3 vaut pour toutes les catégories de cas de l'alinéa 2, et qu'il est impossible d'interpréter unilatéralement.

3. Interprétation systématique    {#systématique}
-----------------------------
Dans une interprétation systématique, on essaye de déterminer le sens d'un texte à partir du contexte de son utilisation et de la conception générale de la loi.

Avec cette méthode, une première étape est de déterminer la signification de la formule « en tant que tel » pour d'autres catégories de cas citées dans le paragraphe 2.

Dans ce paragraphe, les deux premières catégories de cas sont des inventions. Seules les inventions comme telles sont exclues. Cela signifie-t-il que les inventions doivent être séparées en deux sous-catégories, celle des inventions en tant que telles et celle des autres inventions ? Cela ne me semble pas sensé. Il semble plutôt qu'aucune invention n'est un objet brevetable. Cela ne vaut cependant que pour les inventions en tant que telles. Les innovations ne sont pas exclues, car certes elles reposent sur des inventions, mais ne sont justement pas des inventions en tant que telles. Elles ne font que les utiliser.

Si cette compréhension pour la catégorie de l'invention est valide, alors elle signifierait par transposition pour celle du logiciel que celui-ci ne peut pas être partagé en deux sous-catégories, celle du logiciel, et celle du logiciel en tant que tel. Au contraire, aucune forme de logiciel ne serait brevetable, et la restriction signifie seulement que d'autres objets sont brevetables pour eux-mêmes si du logiciel est utilisé pour leur développement.

La deuxième catégorie citée dans le paragraphe 2 est les théories scientifiques. Ici de même, il ne me semble pas possible de construire une partie précise de cet ensemble comme «théorie scientifique en tant que telle», et de l'opposer à une autre partie qui ne serait pas brevetable. Cela vaut mutatis mutandis pour la troisième catégorie, celle des méthodes mathématiques. Pour les trois catégories du numéro un du paragraphe deux, la restriction « en tant que tel », est plutôt facile à comprendre. En effet les inventions, théories scientifiques et méthodes mathématiques peuvent être nommées comme but d'une innovation dans un dépôt de brevet, mais personne ne devrait n'en monopoliser ne fut-ce qu'une partie. Le concept contraire à «en tant que tel» est pour ces trois ensembles « construit avec l'aide des objets nommés ». À la différence du cas du logiciel, le concept «en tant que tel» est dans ces ensembles plutôt facilement compréhensible.

La quatrième catégorie est celle des créations de formes esthétiques (paragraphe 2 alinéa b). Il est difficile de voir quel sens la restriction «en tant que telle» peut avoir. En tout cas, il est ici difficile d'affirmer qu'une partie précise des créations de formes esthétiques peut être opposée à une autre partie brevetable.

Les dix catégories suivantes sont accumulées au paragraphe c du paragraphe trois. Le logiciel arrive après, au dernier rang.

La première de ces dix catégories est les plans pour les activités mentales. Les deuxièmes et troisièmes (Règles et procédés pour les activités mentales) ne se différencient pas de la première de manière significative et peuvent donc être examinés en même temps. Comment doit être comprise la restriction «en tant que telle» ici ? Comme dans les cas précédemment examinés, cette restriction n'exige pas non plus le partage des activités mentales en deux groupes. Aucune activité mentale n'est brevetable.

Les trois catégories suivantes sont les plans, règles et procédés pour les jeux. Les groupes de règles pour les jeux sont à comprendre au plus simple. Tous les jeux ont des règles. Celui qui propose une nouvelle règle ou développe même un tout nouveau jeu ne peut obtenir aucune protection par brevet pour cela. La restriction aux règles «en tant que telles» à travers le paragraphe 3 est dans ce cas particulièrement dure à comprendre. En tout cas elle ne permet pas de déduire une ligne directrice simple pour le cas du logiciel. Cela vaut a fortiori pour les «plans pour des jeux» et les «procédés pour des jeux», qui sont déjà en tant que telles des formulations incompréhensibles.

Le paragraphe deux régit encore deux catégories de cas. Mais on ne doit pas attendre un gain de connaissance supplémentaire et considérable de leur examen. C'est pourquoi je tiens assuré comme résultat d'une interprétation systématique: Dans d'autres catégories de cas importants, l'objet en question ne doit pas être être partagé en deux groupes (« en tant que tel » et « autres »). Avant tout dans les cas réglés par l'alinéa a), cette compréhension du concept « en tant que tel » est plutôt claire. Celle-ci est donc une acceptation voisine pour le logiciel.

4. Interprétation téléologique    {#téléologique}
-----------------------------
L'interprétation téléologique se demande quel est le but (télos en grec) de la loi. Dès lors, parmi plusieurs alternatives interprétatives, celle retenue est celle qui aide le mieux à réaliser ce but.

Une condition préalable est qu'un but issu de la loi et clairement identifiable suive la restriction de l'exclusion des objets et activités cités à aux objets et activités « en tant que tels ».

Le paragraphe 3 concerne quinze ensembles de cas très différents. Dès lors il est difficile d'attribuer un but clairement identifiable au paragraphe 3.

Une interprétation téléologique du paragraphe 3 n'apporte donc aucun gain.

5. Interprétation historique    {#historique}
-----------------------------
L'interprétation historique détermine ce que les personnes participants à la rédaction de la loi ont pensé, pour comprendre le sens d'un texte. À la différence des méthodes utilisées plus haut, on ne travaille pas uniquement sur le texte de la loi, mais on en utilise d'autres (brouillons et protocole de discussion). Ces textes ont été bien compris par Beresford dans son nouveau livre. Les analyses suivantes reposent sur sa présentation.

Il résulte tout d'abord de l'histoire de la constitution du texte, que la restriction universelle «en tant que telle» au paragraphe trois se rapportait dans les brouillons antérieurs seulement aux objets nommés dans l'alinéa a, ie seulement aux inventions, théories scientifiques et méthodes mathématiques. Pour les objets nommés dans l'alinéa c cette restriction était seulement « of a purely abstract nature » (Projet de 1965) ou bien « of a purely intellectual nature » (Projet de 1969). Cela explique la difficulté de l'interprétation du texte actuel. Ce n'est pas une surprise si cette restriction « en tant que tel » n'est pas très claire pour toutes les catégories, car elle n'était originellement pas pensée pour toutes en même temps.

Un projet ultérieur (1971) contient pour la première fois une exclusion de la brevetabilité d'un logiciel. Celle-ci ne contient pas la moindre restriction. Dans ce brouillon, les autres objets nommés dans l'alinéa c sont formulés comme suit: « schemes, rules or methods of doing business, performing purely mental acts or playing games ». Une restriction est prévu seulement pour l'ensemble des « actes mentaux », par « purely ». Ainsi la question de savoir, par exemple, ce qu'il faut comprendre par le procédé d'un jeu est épargné à l'usager de ce texte.

Le groupe de travail a tout d'abord accepté l'exclusion inconditionnelle de la brevetabilité du logiciel. Mais après, sous l'influence de cercles intéressés par cette position, a été décidé la version actuelle qui exclut seulement de la brevetabilité le logiciel « en tant que tel ».

Il est permis de considérer que le résultat de l'interprétation historique est le suivant: La compréhension du concept « en tant que tel » s'est avant tout modelée sur les catégories de cas citées dans le paragraphe a, puisque le concept était originellement pensé pour elles, et donc est la plus claire dans ce contexte. Le résultat de l'explication systématique développée plus haut acquiert ici un poids particulier: Le logiciel ne doit pas être partagé en deux (logiciel comme tel et autre logiciel), au contraire le logiciel est globalement exclu de la brevetabilité. Cette exclusion ne s'étend cependant pas aux innovations qui sont développées avec l'aide de logiciel, de même que l'exclusion des inventions et théories scientifiques ne s'étend pas aux innovations qui appliquent des inventions et théories scientifiques.

6. Interprétation conforme à la constitution    {#constitution}
-----------------------------
Par la méthode de l'interprétation conforme à la constitution, on se demande quelles conséquences ont en terme de droit constitutionnel différentes conclusions, et on choisit la conclusion la plus compatible avec les valeurs de la constitution. Avant tout, ce sont les droits fondamentaux qui doivent être pris en considération.

En pratique, le principe fondamental le plus important dans la constitution allemande est celui d'égalité, Article 3 Paragraphe 1.

Ce principe interdit de traiter différemment pour des raisons factuelles ce qui est essentiellement semblable.

Parmi les quinze catégories du paragraphe 2, celles des logiciels et les créations de formes esthétiques sont dans ce contexte semblables, puisqu'elles sont déjà protégés par le droit d'auteur. Cela n'est pas le cas pour tous les autres objets et activités. Une interprétation orientée par l'article 3 paragraphe 1 de la loi fondamentale doit donc avant tout faire attention à ce que le traitement du logiciel ne diffère pas de celui de la création de formes esthétiques sans raison factuelle. À l'inverse il faut fournir une raison factuelle pour soutenir l'hypothèse que le logiciel doit être protégé doublement par le droit d'auteur et le droit sur le brevet, à la différence des autres objets d'innovation. Or puisque les brevets pour des créations de formes esthétiques ne sont pratiquement jamais délivrés, ce point de vue exige, au sujet du logiciel, une conception soigneusement délimitée du domaine d'exception du paragraphe 3.

Ensuite, l'interprétation doit prendre en compte l'article 103 alinéa 2 de la constitution (NdT: ici il s'agit de la constitution allemande). Il y est édicté qu'un fait ne peut être puni que si la pénalité était légalement certaine avant son accomplissement. Cela interdit l'élargissement des interdictions en droit pénal à travers la jurisprudence ou le droit coutumier. C'est pertinent dans ce contexte car l'article 142 de la loi allemande sur le brevet prévoit des règles de pénalisation pour l'infraction des brevets. Cela signifie que chaque attribution d'un brevet peut se référer aux retombées pénales édictées à cet endroit. Ces retombées ne sont cependant édictées par le législateur que dans la mesure où celles-ci sont sans effets sur les causes d'exception, identiques à l'article 52 de la CEB, dans l'article 1 paragraphe 2 de la loi allemande sur le brevet. Une délivrance de brevet incompatible avec les termes de ces causes d'exception enfreint alors le principe de légalité. Dans une interprétation fidèle à la constitution, une valeur particulière doit donc être octroyée à la formulation du législateur. Une interprétation qui aménage de fond en comble cette formulation en suivant les désirs des tribunaux n'est pas seulement illégale, mais en plus inconstitutionnelle.

Finalement l'article 14 de la loi fondamentale joue probablement ici un rôle. Cet article inclut la garantie constitutionnelle de la propriété. Le paragraphe 1 stipule que la propriété est garantie, mais que son contenu et ses restrictions doivent être édictées par le législateur. Dès lors il serait vraisemblable de stipuler, sans aller plus avant, que dans l'ensemble le traité sur le brevet l'abolit rétroactivement. (Au contraire d'une rétroactivité qui ne permettrait plus aucune requête à partir d'une date déterminée). Mais sur cette question de l'interprétation de l'article 52 de la CEB au sujet de la brevetabilité, il ne s'agit pas de trancher de manière aussi radicale. Si le groupe de travail avait, suivant le projet de 1971, exclu sans restriction le brevet du champ du brevetable, il n'aurait alors presque pas été entravé par l'article 14 de la loi fondamentale. Et quand aujourd'hui l'interprétation conclut que le logiciel est excepté dans une large mesure de la protection par brevet, cela n'engendre aucun empiètement sur la propriété de l'innovateur que la constitution interdise. Cela est d'autant plus valable que le logiciel est déjà protégé par le droit d'auteur. Ainsi une exclusion du champ du brevetable ne signifie aucunement une exclusion de toute valorisation économique.

7. Résultat intermédiaire    {#intermédiaire}
-----------------------------
L'interprétation du texte arrive déjà à la compréhension qu'il ne reste aucun domaine d'application pour l'exclusion du logiciel du champ du brevetable, car celle-ci n'est pas compatible avec la formulation «dans la mesure où».

Il découle de l'interprétation systématique, particulièrement soutenue par l'interprétation historique, que le logiciel n'est pas divisible en deux parties, le logiciel « en tant que tel », et les autres logiciels, mais que comme dans le cas des inventions et des théories scientifiques tous les logiciels sont exclus du champ du brevetable. Mais les innovations développées à l'aide de logiciels sont brevetables.

L'interprétation conforme à la constitution interdit un traitement différent des créations de formes esthétiques sans raisons factuelles et un élargissement du champ de la brevetabilité sans considération pour les limites du texte développées plus haut.

8. Discussion des pratiques de l'Office européen des brevets    {#pratiques}
-----------------------------
La pratique de l'office européen des brevet suit pour l'instant la ligne de la décision 3.5.1 du premier juillet 1998 de la chambre de recours spécialisée.

À la page 12 de l'impression, la cour d'appel signifie que les programmes d'ordinateur sont divisibles en deux parties, ie en logiciels en tant que tels, et en autres logiciels. Cette conception n'est pas compatible avec les résultats obtenus plus haut par une interprétation systématique. Elle n'est pas plus détaillée, mais est déduite de considérations superficielles sur les paragraphes 2 et 3. Dans cette décision on ne trouve aucune application conforme aux méthodes d'interprétation juridique mises en œuvre plus haut.

La chambre stipule à la page suivante que la restriction « en tant que tel » doit être comprise ainsi: seuls les programmes d'ordinateur sans caractère technique sont des programmes comme tels.

C'est tellement éloigné des termes du texte qu'une punition pour un brevet délivré à la suite de cette interprétation illégale entre en contradiction avec le principe de légalité (article 103 paragraphe 2 de la loi fondamentale). C'est une formulation complètement nouvelle de la barrière du paragraphe 3, qui n'a plus rien de commun avec les termes de la loi. La chambre de recours spécialisée outrepasse là clairement les limites de son activité juridique. Celui qui aimerait remplacer la formulation « en tant que tel » par « sans caractère technique » produit nécessairement un changement du texte du traité. La jurisprudence n'y est pas autorisée.

La conférence gouvernementale de l'automne 2000 a décidé d'inscrire dans l'article 1 du caractère 52 l'exigence d'un caractère technique. Le paragraphe 1 devrait proclamer dans le futur: « European patents shall be granted for any inventions, in all fields of technology, provided that they are new, involve an inventive step and are susceptible of industrial application ». L'exigence « in all fiels of technology » n'était pas l'inscription de quelque chose de nouveau dans le paragraphe 3, et la restriction actuelle « as such » était abandonnée, de même que la jurisprudence l'a fait pour l'ensemble de cas qu'est le logiciel en outrepassant ses compétences. Ce n'est qu'après le nouveau texte que le caractère technique peut être considéré comme un critère décisif, si on est de l'avis que cet examen doit être effectué deux fois dans le paragraphe 1 et le paragraphe 3, ce qui ne serait pas pertinent d'un point de vue systématique.

Une courte considération systématique montre que l'opinion de la chambre de recours technique est problématique dès le départ. Elle devrait en effet aussi partager le domaine des inventions et des théories scientifiques en deux groupes, « avec caractère technique » et « sans caractère technique », et rendre possible une monopolisation d'une partie de la science par des brevets.

Les réflexions ultérieures de la cour d'appel concernent la question de savoir comment doit être partagé le domaine du logiciel en deux morceaux d'après son critère de « caractère technique » contradictoire avec les termes du texte et avec un critère développé systématiquement. Cette réflexion conduit à une reconnaissance pleine et entière de la brevetabilité du logiciel. La distinction « logiciel en tant que tel » est pour la cour d'appel d'une signification évanescente, ce qui n'est pas compatible avec la formulation « seulement dans la mesure où ».

La tentative d'une justification téléologique est très explicative, car la cour d'appel l'entreprend page 21. La cour d'appel y conclut: « Le but et la fin de la CEB est bien de permettre de délivrer des brevets pour des inventions et d'inciter par une protection convenable de ces inventions au progrès technique. Pour cette raison et à la lumière de ce qu'est une invention, la cour est tenue aux interprétations précédentes, d'autant plus que les techniques de l'information envahissent tous les domaines de la société et fournissent des inventions on ne peut plus précieuses. »

Cela relève au contraire du manque de preuve antérieure de l'interprétation téléologique. Il s'agit ici de l'interprétation du paragraphe 3 de la CEB. Le but de celle-ci, comme montré ci-dessus, n'est pas objectivement et évidemment net, au regard du nombre et de la diversité des clauses d'exclusion. L'affirmation que la limitation de l'exclusion du logiciel du champ de la brevetabilité au seul logiciel en tant que tel a pour but, au paragraphe 3, d'inciter au progrès technique au regard du développement des techniques de l'information, par la reconnaissance de sa brevetabilité, n'est pas convaincante. Dans le cas où le législateur aurait suivi un tel but, il n'aurait a priori pas prévu l'exception au paragraphe 2. Si certes la présomption que le but du législateur coïncide avec le résultat désiré n'est pas correcte, cela montre cependant la propension de la cour d'appel technique à faire prévaloir ses positions propres sur celles du législateur.

Lectures complémentaires (recommandations de l'éditeur)    {#complémentaires}
-----------------------------
J'ai laissé les liens et titres en allemand et en anglais, en supposant que ceux qui iront les lire devront de toute manière comprendre ces langues.


*	<a href="http://www.k.lenz.name/LB/">Lenz Blog</a>
*	<a href="http://k.lenz.name/LB/archives/000617.html">Kflenz-horns0309</a>
*	 <a href="http://k.lenz.name/d/v/Grenzen.pdf">Lenz 2002-03-01: Grenzen der Patentierbarkeit</a>

	German book by Dr. Karl-Friedrich Lenz, professor (kyôju) of European Law, which explains many aspects of the current debate on patentability, mostly in German, and also contains some english texts, including a chapter on the CEC/BSA directive proposal of 2002-02-20.

*	<a href="http://k.lenz.name/LB/archives/000264.html">Lenz 2002-03-01: Sinking the Software Patent Proposal</a>

	Karl-Friedrich Lenz, professor of European Law, lists some legal and constitutional arguments to explain why the CEC/BSA proposal is a legal and political scandal, starting from the fact that the European Commission is using  “harmonisation” and  “clarification” merely as pretexts to declare itself competent for promoting an unspeakable political agenda which does not fall in the Commission's competentce.

*	 <a href="http://eupat.ffii.org/papers/melullis02/index.de.html">Melullis 2002: Zur Sonderrechtsfähigkeit von Computerprogrammen</a>

	This article by the presiding judge of Germany's highest patent senate approvingly quotes the text and arguments of Prof. Lenz.

*	<a href="http://eupat.ffii.org/papri/epo-t920769/index.en.html">Art 52 EPC: Interpretation and Revision</a>

	The limits of what is patentable which were laid down in the European Patent Convention of 1973 have been eroded over the years. Influential patent courts have interpreted Art 52 in a way that renders it obscure and meaningless. Not all courts have followed this interpretation, and numerous law scholars have shown why it is not permissible. The EPO had accepted the inconsistencies in anticipation of an expected change of law. However this expectation was frustrated in 2000 by the governments and in 2003 by the European Parliament. The Parliament voted for a clarification which gives Art 52 back its meaning. Meanwhile, proponents from all sides have proposed to modify Art 52(3) EPC in one or the other way, of course while claiming that this merely serves to  “clarify the status quo” or to implement a directive which serves this purpose, and, since the European Commission and the Council have not signalled support for the Parliament's approach, there is still no common understanding of which  “status quo” we are talking about.

*	<a href="http://eupat.ffii.org/analysis/korcu/index.en.html">Patent Jurisprudence on a Slippery Slope &#8212; the price for dismantling the concept of technical invention</a>

	So far computer programs and other *rules of organisation and calculation* are not *patentable inventions* according to European law. This doesn't mean that a patentable manufacturing process may not be controlled by software. However the European Patent Office and some national courts have gradually blurred the formerly sharp boundary between material and immaterial innovation, thus risking to break the whole system and plunge it into a quagmire of arbitrariness, legal insecurity and dysfunctionality. This article offers an introduction and an overview of relevant research literature.

*	<a href="http://eupat.ffii.org/index.en.html">Software Patents vs Parliamentary Democracy</a>

	We explain the state of play regarding state-granted idea monopolies, specially in the context of the draft directive  “on the patentability of computer-implemented inventions” (software patent directive), which has become a test case on the extent to which parliaments have a say in contemporary European legislation.

***

<sub>Voir aussi la <a href="http://eupat.ffii.org/analysis/epc52/exeg/index.en.html">version anglaise</a> du texte.</sub>
