Title: Réponse du Chaos Computer Club aux inepties des scénaristes de séries policières
Date: 2012-04-01 10:00:15
Summary: Chers scénaristes de séries policières,
Source: <a href="http://ccc.de/updates/2012/drehbuchautoren"><i class="icon-external-link"></i> Antwort auf den offenen Brief der Tatort-Drehbuchschreiber</a>
Author: Chaos Computer Club

*Chers scénaristes de séries policières,*

C'est avec joie (et très chiquement comme représentants des « Communautés du Netz » que vous avez pourfendues) que nous avons pris connaissance de [votre attention à nos réflexions](http://politiquedunetz.sploing.fr/index.html%3Fp=1375.html) pour rendre objectives les discussions sur les droits des auteurs et de leurs ayants-droits à l'ère du numérique. Avant d'ouvrir le dossier cependant : Nous aussi sommes des auteurs, même des auteurs professionnels, pour être précis. Nous sommes des développeurs, des hackeurs, des organisateurs, des musiciens, des auteurs de livres et d'articles, nous publions même nos propres journaux, blogs et podcasts. Il ne s'agit donc pas seulement pour nous de discuter avec des auteurs, nous-même sommes des auteurs.

C'est pourquoi il n'y aura pas de compromis historique, parce qu'il n'y a pas deux côtés à réconcilier, en tout l'opposition auteurs-récepteurs ne tient pas. Ce qui existe est des ignorants (au mieux) pré-numériques et fétichistes des sociétés d'exploitation d'un côté et vous et nous de l'autre, qui nous voyons nous faire imposer vos contrats.

Le tragique (au sens grec) est cependant que nous sommes tous les deux des victimes du système des sociétés de gestion des droits d'auteur. Cela fait tant d'années que vous vous échinez pour l'industrie de l'exploitation des droits d'auteur, pour laquelle vous avez au fur et à mesure abandonné un bon nombre de vos droits, que vous ne tirez aucun bénéfice pour vous ou votre descendance des allongements de la durée du droit d'auteur. C'est simplement un moyen de négociation avec lequel vous espérez réduire l'ampleur de votre arnaque par les sociétés de gestion. En fait nous combattons du même côté, mais vous ne l'avez jamais remarqué.

Pour nous la situation est très différente. Beaucoup de logiciels ne sont même plus écrits pour gagner de l'argent, mais mis gratuitement voir librement à disposition sur Internet parce qu'il est certain pour les auteurs qu'ils ne gagneront pas un seul centime de leur travail. Il n'existe pas de société de gestion des droits d'auteurs pour les développeurs, par faute de précédent historique. Et si vous regardez, vous verrez vite qu'aucun d'entre nous ne demande la formation d'une GEMA (ou SACEM/SABAM en France/Belgique). Nous ne vous privons pas de ce que nous exigeons pour nous. Nous avons seulement pris congé de l'idée que ce modèle existera encore dans 10 ans.

Dans le domaine commercial le logiciel est généralement produit sur commande ou par des employés, et tous les droits d'exploitation vont au fournisseur du contrat. Ça vous dit quelque chose ? C'est simplement que personne ne cherche à ce que quelqu'un représente nos droits.

Et savez-vous quel est le domaine créatif qui croît le plus vite et fait le plus de chiffre d'affaire, le vôtre ou le nôtre ? Surprise : il semble que l'on puisse survivre sans société de gestion des droits d'auteur. Au lieu de rendre les consommateurs coupables, vous devriez utiliser tous vos efforts pour être correctement rémunérés pour vos œuvres auprès de vos clients. Ce dont vous avez besoin est un syndicat solide au nom vendeur, pas d'un monstre de société de gestion, qui porte plainte auprès de Youtube pendant des années parce qu'ils vous <a href="http://www.ccc.de/de/updates/2011/kulturwertmark">font de la pub</a> gratuitement et que vous trouvez des contrats grâce à ça.

Afin de trier les invectives dans votre lettre ouverte :

1.	« Nous » appellerions de nos vœux une période apocalyptique de néant culturel à la « Demolition Man » pour nous procurer un accès gratuit à la culture.
2.	« Nous » ne reconnaîtrions pas que les fournisseurs de services culturels puissent accumuler la propriété de leurs créations. 

Rapidement, nos réponses:

1. L'ennemi est plutôt les exploitants intéressés uniquement par l'argent et pas par la culture
2. La critique globale des sociétés de gestion que vous nous avez rabâchée avec des commentaires pittoresques sur des propos sortis de leur contexte venant de trois partis différents et d'une « Communauté du Netz » n'a jamais été exprimée comme vous l'entendez. Nous n'avons jamais rencontré ce style de discussion pompeux que chez les gosses de douze ans qui mettent toute l'industrie du divertissement dans le même panier, qui se rebellent contre la GEMA, l'État et trop peu d'argent de poche, téléchargent pour rien leur musique sur Internet et utilisent toutes les justifications pour ce. Que l'on n'ait pas encore trouvé d'équilibre dans ce champ mouvant entre les nouvelles technologies et la création culturelle d'avant le Netz, c'est évident. Ce n'est pourtant pas une raison pour jeter tous les internautes dans le même panier.

Bien sûr personne n'affirmera que Chostakovitch est énormément partagé sur les réseaux d'échange. Ce n'est pas un mensonge, même si c'est dommage. Que la duplication non-souhaitée de produits numériques devienne un problème de société a moins à voir avec le droit d'exploitation à réformer qu'avec ces folies de mises en demeure qui pour l'instant choppent surtout de manière très fraîche des jeunes consommateurs potentiels qui ne trouvent pas leur bonheur autrement.

Le raccourcissement de la durée légale du droit d'auteur est aussi un des outils pour justement vous (ou plutôt vous et nous) débarrasser des obstacles lors de l'exercice de nos métiers. À présent vous devriez quand même comprendre (vu la quantité très faible d'intrigues policières), <a href="http://www.zeit.de/2012/13/Krimi-Tatort/komplettansicht">que les invectives pour plagiat</a> lors de la réutilisation de bouts d'histoire sont un horrible champ de mine. Nous, comme auteurs de logiciel, cela fait longtemps que nous nous déplaçons dans un environnement miné par les brevets, et nous voyons très bien où le train mène.

Vous devriez dès lors enlever vos doigts de la durée de protection légale. Oh, s'il vous plaît, vite ! Nos oreilles saignent à cause de toutes ces mélopées que nous devons écouter répétitivement depuis des années parce que leur durée de protection a été allongée. Nous sommes presque arrivés à un siècle de protection, et c'est maintenant que vous déclarez qu'on ne devrait pas y toucher ? Nous croyons que ça hacke (veut aussi dire fendiller en allemand). Voilà l'ère du numérique, mes amis, et nous ne savons même pas comment garder des données numériques pendant un siècle. Les archives et bibliothèques n'en ont pas encore la moindre idée. Et si ces problèmes de DRM et ces manques de formats ouverts, ce sont les vrais problèmes, et que les deux ont un lien avec la durée de protection, ce n'est pas qu'à cause d'eux que vous devez radicalement diminuer la durée du droit d'auteur, mais aussi parce que vous vous êtes vous-même perchés sur les épaules de géants auxquels vous devez de toute façon payer votre tribut.

À ce propos, Sir Arthur Conan Doyle écrivait :

>« Si chaque auteur qui est redevable de sa production à Poe et reçoit des honoraires pour une histoire devait en donner un dixième pour l'érection d'un monument à la mémoire du maître, la pyramide serait aussi grande celle de Kéops »

La dite « propriété intellectuelle » que vous posez comme si elle était un don divin est à y regarder de plus près une chimère de la dernière pluie, qui est volontiers utilisée comme un terme subjectivement polémique pour éviter les discussions de fond. Dans les dernières années, beaucoup de commentaires à ce propos ont été rédigés, parfois de manière très équilibrée [^1].

Que quelques sociétés de gestion soient dépassées par le simple fait que les copies des œuvres ne puissent pas être empêchées, ne change rien au fait que maintenant comme avant il existe une réelle propension à rémunérer les fournisseurs de services culturels de manière appropriée. Quand il existe des possibilités d'acheter sans stress et sans contraintes des œuvres à des conditions honnêtes, celles-ci sont abondamment utilisées, que ce soit des catalogues d'application pour portables ou des magasins de musique en ligne avec des grilles tarifaires simples. Le commerce des livres numériques est en route, et quand il se traîne c'est d'ailleurs à cause de système de DRM trop jeunes. Dans les communautés du Netz des concepts comme Flattr permettent de rémunérer <a href="http://cre.fm/">même les simples citations</a>.

Que cette transition ne soit pas simple pour chacun, nous voulons bien le comprendre. Et le droit de recours collectif, cela fait du reste des années que nous le demandons. Et qui nous a trahi ? Bien vu.

Particulièrement vous comme auteurs de romans policiers, qui gagnez surtout votre pain grâce à la redevance télévisuelle, vous devriez savoir à quoi ressemble une licence globale. Les auteurs n'en meurent pas. Mais cette société de gestion qui vous paye vos scénarios est un excellent exemple pour apprendre comment une usine à gaz qui ne sert qu'elle-même s'annexe de plus en plus vos parts des bénéfices sur les émissions diffusées. Lever la main, combien d'entre vous sont en CDI ? Combien d'entre vous ont été dépouillés dans les dernières années par des changements de contrat dans les médias régionaux ou par la spoliation des droits de rediffusion ?

Encore quelque chose : Nous devrions discuter avec nos politiques de la culture, vous dites. Quelle bonne idée, comme si nous ne papotions pas déjà d'oseille avec eux depuis une décennie. À ce propos nous devons faire quelques petites précisions pour distinguer entre les différents partis que vous avez amalgamés : Discuter avec les Verts, c'est discuter avec un mur. Ils écoutent rarement, n'ont pu appliquer aucune idée adaptée à l'ère du temps dans les dernières années concernant le droit d'auteur et ont une attitude fondamentalement conservatrice, que même Ansgar Heveling (conservateur catholique) adorerait. Les Pirates n'ont aucune personnel dédié à la culture. Die Linke sont contrairement aux rumeurs les plus progressistes, ceux qui réfléchissent aux nouvelles idées. Bien sûr, comme il n'ont aucun pouvoir pour appliquer ces idées ils en discutent d'autant plus facilement.

Maintenant, vous avez une idée de ce que nous faisons avec nos bras ? Exactement.

[^1]: Ansgar Ohly, Diethelm Klippel (Hg.): Geistiges Eigentum und Gemeinfreiheit, Tübingen: Mohr Siebeck 2007


