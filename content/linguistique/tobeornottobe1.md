Title: Être ou ne pas être : des aspects de l'existence (Acte 1)
Date: 2013-04-16 17:00:15
Source: <a href="http://langevo.blogspot.fr/2013/03/to-be-or-to-be-aspects-of-existence.html"><i class="icon-external-link"></i> To Be or to Be: Aspects of Existence (Act I)</a>
Author: Piotr Gąsiorowski
Summary: Bien que le sanskrit ásti et sánti, le latin est et sunt et le viel anglais is et sind paraissent différents, historiquement parlant ils ne sont que des formes différentes du même verbe. 

La conjugaison du verbe être en anglais est un exemple instructif d'un paradigme créé par une coalition de différentes formes verbales avec différentes histoires. Nous attendons normalement d'un verbe que sa conjugaison consiste en la répétition d'une même racine avec des terminaisons différentes. Par exemple, le latin laudare, «louer, prier», se conjugue comme suit au présent à la voix active. 

	laudō, laudās, laudat, laudāmus, laudātis, laudant

Les passifs correspondant sont :

	laudor, laudāris, laudātur, laudāmur, laudāminī, laudantur

Et le subjonctif plus-que-parfait donne: 

	laudāvissem, laudāvissēs, laudāvisset, laudāvissēmus, laudāvissētis, laudāvissent

On pourrait continuer ainsi longtemps. Tous les temps contiennent la racine laud- à laquelle est attachée le suffixe caractéristique de la première conjugaison, -ā- pour former la forme de base laudā-. Un tel paradigme est uniforme génétiquement et également hautement régulier. 

Mais d'autres paradigmes sont nettement moins réguliers. Par exemple prenons le verbe être au présent dans plusieurs langues indo-européennes.

<table border="1" cellpadding="1" cellspacing="1">
 <tbody>
<tr>
  <td>
Langue
</td>
  <td>
1sg.
</td>
  <td>
2sg.
</td>
  <td>
3sg.
</td>
  <td>
1pl.
</td>
  <td>
2pl.
</td>
  <td>
3pl.
</td>
 </tr>
<tr>
  <td>
<a href="http://fr.wikipedia.org/wiki/Latin">Latin</a>
</td>
  <td>
sum
</td>
  <td>
es
</td>
  <td>
est
</td>
  <td>
sumus
</td>
  <td>
estis
</td>
  <td>
sunt
</td>
 </tr>
<tr>
  <td>
<a href="http://fr.wikipedia.org/wiki/Grec_ancien">Grec ancien</a>
</td>
  <td>
eimí
</td>
  <td>
eî
</td>
  <td>
estí
</td>
  <td>
esmén
</td>
  <td>
esté
</td>
  <td>
eisí
</td>
 </tr>
<tr>
  <td>
<a href="http://fr.wikipedia.org/wiki/Gotique">Gotique</a>
</td>
  <td>
im
</td>
  <td>
is
</td>
  <td>
ist
</td>
  <td>
sijum
</td>
  <td>
sijuþ
</td>
  <td>
sind
</td>
 </tr>
<tr>
  <td>
<a href="http://fr.wikipedia.org/wiki/Vieux-slave">Vieux-slave</a>
</td>
  <td>
jesmĭ
</td>
  <td>
jesi
</td>
  <td>
jestŭ
</td>
  <td>
jesmŭ
</td>
  <td>
jeste
</td>
  <td>
sǫtŭ
</td>
 </tr>
<tr>
  <td>
<a href="http://fr.wikipedia.org/wiki/Sanskrit">Sanskrit védique</a>
</td>
  <td>
ásmi
</td>
  <td>
ási
</td>
  <td>
ásti
</td>
  <td>
smás
</td>
  <td>
sthá
</td>
  <td>
sánti
</td>
 </tr>
<tr>
  <td>
<a href="http://fr.wikipedia.org/wiki/Hittite_(langue)">Hittite</a>
</td>
  <td>
ēsmi
</td>
  <td>
ēssi
</td>
  <td>
ēszi
</td>
  <td>
ēsweni
</td>
  <td>
ēsteni
</td>
  <td>
asanzi
</td>
 </tr>
</tbody></table>

L'existence de similitudes est assez frappante, et il est aussi clair que les langues indo-européennes ont en commun de nombreuses irrégularités dans la conjugaison de ce verbe. Ainsi, si nous comparons les troisièmes personnes du singulier et du pluriel, il est assez clair que la forme originale a dû ressembler à dans une hypothétique langue proto-indo-européenne (PIE)[^1] : 

1)	3sg.\*<b>es-ti</b>, 3pl. \*<b>s-Vnti</b> {@style=text-align: center;}

où \*<b>V</b> est une voyelle difficile à déterminer. Quelques formes semblent tendre vers un e, d'autres vers un o. 

Le védique suggère que la racine a la forme es- au singulier et s- au pluriel car cela cadrerait avec certaines régularités dans les intonations. Les indo-européanistes préfèrent généralement parler d'accent parce qu'ils pensent que la différence d'intonation entre les syllabes était initialement marquée par des différences de hauteur de ton plutôt que simplement une prononciation plus marquée dans l'hypothétique ancêtre commun aux langues indo-européennes. Cependant, il doit y avoir eu un moment où cette intonation particulière a dû être corrélée avec un effort articulatoire et où l'accent était en fait une manière d'insister dynamiquement comme cela se fait en anglais moderne. L'absence d'insistance entraînait souvent une réduction phonétique ou la perte complète de la voyelle. Dans ce cas, la voyelle de la racine \*<b>es </b> était réduite à zéro, en laissant seulement \*<b>s</b> si l'insistance se faisait sur la conjugaison de fin et non sur la racine. 

La reconstruction de 1 était celle reconnue comme standard à la fin du 19<sup>ème</sup> siècle. Lors des dernières décennies, la compréhension de la grammaire du PIE a progressé et à présent nous utilisons une reconstruction plus sophistiquée, en réécrivant la racine \*<b">h&#8321;es-</b>, et en supposant que l'intonation de fin pouvait se déplacer entre la racine et la terminaison de la troisième personne du pluriel, \*<b>-enti</b> :

2)	3sg. \*<b>h&#8321;és-ti</b>, 3 pl. \*<b>h&#8321;s-énti</b> {@style=text-align: center;}

<table cellpadding="0" cellspacing="0" style="float: left; margin-right: 1em; text-align: left;"><tbody>
<tr><td style="text-align: center;"><a href="http://www.cdli.ucla.edu/dl/photo/P393110.jpg" imageanchor="1" style="clear: left; margin-bottom: 1em; margin-left: auto; margin-right: auto;"><img border="0" height="400" src="http://www.cdli.ucla.edu/dl/photo/P393110.jpg" width="225" /></a></td></tr>
<tr><td class="tr-caption" style="text-align: center;">
Un <a href="http://cdli.ucla.edu/cdlisearch/search/index.php?SearchMode=Text&amp;txtID_Txt=P393110">texte hittite rituel</a>
</td></tr>
</tbody></table>


\*<b>h&#8321;</b> est une sorte des dites «<a href="http://fr.wikipedia.org/wiki/Th%C3%A9orie_des_laryngales"><b>PIE laryngales</b></a>». Il s'agit de consonnes perdues dans la plupart des langues de la famille, même s'il en reste des traces. L'existence de ces consonnes fut supposée dans les années 1870, et fut démontrée plusieurs années plus tard. C'est un cas intéressant, qui montre que la linguistique historique peut faire des hypothèses testables. Trois phonèmes semblables à des h sont généralement reconstitués pour le PIE. On les symbolise  \*<b>h&#8321;</b>, \*<b>h&#8322;</b>, and \*<b>h&#8323;</b> pour éviter de se prononcer quand à leur valeur phonétique précise, mais la plupart des linguistes pensent que \*<b>h&#8321;</b> était glottique, comme le son /h/ en anglais, tandis que  \*<b>h&#8322;</b> and \*<b>h&#8323;</b> demandaient une articulation du fond de la gorge, vélaire, uvulaire ou pharyngéale, et que \*<b>h&#8323;</b> était probablement vocalisé. Dans quelques langues comme le grec, une laryngéale avant une consonne se transforme en voyelle, mais cela a disparu dans la plupart des autres langues. C'est pourquoi le védique a comme troisième forme du pluriel sánti tandis qu'en grec la proto-forme \*<b>h&#8321;s-énti</b> est devenue \*<b>esenti</b>, puis  \*<b>ehensi </b>. Cette dernière forme reflète la prononciation probable du mycénien archaïque, qui utilisait le [linéaire B](http://fr.wikipedia.org/wiki/Lin%C3%A9aire_B) comme système d'écriture et dans lequel cela s'écrit e-e-si, tandis qu'en grec classique c'est eisi. 

Il y avait aussi d'autres verbes de cette sorte: 

3)	3sg. \*<b>gʷʰén-ti</b>, 3pl. \*<b>gʷʰn-énti</b> «frapper, tuer» {@style=text-align: center;}

Ils n'ont pas l'air particulièrement irréguliers quand vous regardez la reconstruction du PIE. Cependant, puisque la présence ou l'absence d'une voyelle complète fait souvent la différence dans leur développement ultérieur, le contraste entre la forme 3sg. et celle 3pl. s'est accru dans les langues filles. Par exemple, le védique ressemble à: 

4)	3sg. hánti, 3pl. gʰnánti {@style=text-align: center;}

où le h du singulier, prononcé comme la fricative glottique semi-vocalisée /ɦ/ représente le résultat en védique d'une vélaire aspirée qui a arrêté d'être palatisée devant une voyelle. Le développement complet a du ressembler à:  

5)	\*<b>gʷʰén-ti</b>;&rarr; \*<b>gʰénti</b>&rarr;\*<b>ǰʰénti</b> &rarr; *<b>ǰʰánti</b> &rarr; hánti {@style=text-align: center;}

Notez que bien que *<b>e</b> soit devenu une voyelle centrale basse (\*<b>a</b>)  dans les langues indiennes, elle a d'abord affecté la prononciation des consonnes précédentes, ce qui trahit sa qualité originale de voyelle frontale.

Donc, bien que le sanskrit ásti et sánti, le latin est et sunt et le viel anglais is et sind paraissent différents, historiquement parlant ils ne sont que des formes différentes du même verbe. Mais d'où viennent les formes be et been ? En vieil anglais, le verbe «to be» se conjuguait comme suit: 

6)	1sg. bēo, 2sg. bist, 3sg. biþ; pl. bēoþ {@style=text-align: center;}

Il semble qu'il n'y a aucune rapport avec la racine *<b>h&#8321;es</b>. D'où cela vient-il? Le prochain billet répondra à cette question. 

[^1]:	L'astérisque sert à signaler qu'il s'agit d'une reconstruction. 
