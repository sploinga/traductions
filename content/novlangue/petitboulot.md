Title: Légère occupation
Date: 2013-04-09 15:00:15
License: <a href="http://creativecommons.org/licenses/by-nc-sa/2.0/de/"><i class="icon-legal"></i>CC-BY-NC-SA</a>
Source: <a href="http://neusprech.org/geringfuegig-beschaeftigt/"><i class="icon-external-link"></i> beschäftigt, geringfügig</a>
Author: Martin Haase

Nous sommes occupés quand nous nous activons, quand nous avons affaire à quelque chose, id est quand nous nous sentons très pris. L'expression [l. o.](http://fr.wikipedia.org/wiki/L%C3%A9g%C3%A8re_Occupation) exploite complètement cette signification (pour dissimuler que les activités en question ne nous prennent justement pas tout notre temps). C'est pourquoi la langue courante allemande a formé l'expression minijob pour ce type de travail. Elle est plus pertinente, mais aussi fausse en un certain sens. Parce que contrairement à ce qu'elle semble vouloir dire, elle ne renvoie pas à la quantité de travail mais à la faiblesse du salaire. D'après le [code des lois sociales](http://www.sozialgesetzbuch-sgb.de/sgbiv/8.html), il existe une «légère occupation» quand «la rémunération ne dépasse pas plus de 450€ mensuels». Et ce indépendamment du montant horaire trimé. Avoir une l. o., ça signifie donc être payé que dalle pour ses efforts. De ce point de vue, l'expression n'est pas seulement un euphémisme, elle déchire aussi involontairement le masque, car elle signale qu'il faut la prendre au second degré. En Allemagne, il n'y pas que les travailleurs actifs qui sont occupés, mais on occupe aussi les enfants pour qu'ils ne s'ennuyent pas. Il s'agit donc de faire faire quelque chose qui ressemble à du travail mais ne mérite pas l'appellation de métier. 



