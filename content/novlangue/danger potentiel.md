Title: Danger potentiel
Date: 2012-03-22 10:04:15
License: <a href="http://creativecommons.org/licenses/by-nc-sa/2.0/de/"><i class="icon-legal"></i>CC-BY-NC-SA</a>
Source: <a href="http://neusprech.org/gefaehrder-potenzieller"><i class="icon-external-link"></i> Gefährder, potenzieller</a>
Author: Kai Biermann

Comparatif. Un homme qui n'est pas encore devenu un <a href="http://novlangue.sploing.fr/danger/">D.</a>, mais pourrait le devenir. Mais contre qui on peut prendre des « <a href="http://novlangue.sploing.fr/mesure/">Mesures</a> ». Tout le monde est un D. potentiel. Tout compte fait chaque citoyen peut devenir dangereux dans le futur. Dans d'autres contextes on appelle ça de la suspicion généralisée.

