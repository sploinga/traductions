Title: Professionnalisant
Date: 2013-04-09 15:00:15
License: <a href="http://creativecommons.org/licenses/by-nc-sa/2.0/de/"><i class="icon-legal"></i>CC-BY-NC-SA</a>
Source: <a href="http://neusprech.org/berufsqualifizierend"><i class="icon-external-link"></i> berufsqualifizierend</a>
Author: Martin Haase

Comme ça sonne bien, quand un cursus est professionnalisant ! Malheureusement ce n'est pas toujours vrai, du moins littéralement, car l'obtention d'un diplôme niveau licence ne qualifie pas pour un métier, mais montre bien plutôt qu'on va pouvoir apprendre un métier pour lequel un niveau supérieur au bac était nécessaire. Il serait donc plus pertinent d'utiliser le mot «employable». D'ailleurs, c'est ainsi que l'on dit en anglais, et le mot, peut-être par anglicisme, est aussi utilisé en français. Même si en Angleterre l'emploi du mot et sa signification précise sont tout particulier : Là-bas il sert à parler des mesures d'aide sociale visant à rendre employable des jeunes «à problème», sociaux ou psychologiques, qui justement ne le sont donc pas. Nous nous éloignons largement du contexte allemand, car personne n'ira qualifier une licence de mesure d'aide psycho-sociale. 

