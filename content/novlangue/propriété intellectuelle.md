Title: Propriété intellectuelle
Date: 2012-03-22 10:04:15
License: <a href="http://creativecommons.org/licenses/by-nc-sa/2.0/de/"><i class="icon-legal"></i>CC-BY-NC-SA</a>
Source: <a href="http://neusprech.org/geistiges-eigentum"><i class="icon-external-link"></i> Geistiges Eigentum</a>
Author: Kai Biermann

L'expression paraît si naturelle que c'est forcément du vocabulaire de propagande qui sert uniquement à provoquer des associations d'idées[^1]. Car l'idée de la PI ne fait pas partie des idées qui fonctionnent. Elle est même contre-productive. P. fait référence à des trucs qui tirent leur valeur du fait que quelqu'un jouit seul de leur maîtrise. De l'argent inutilisé peut valoir beaucoup quand il fait partie d'un trésor, parce qu'à ce moment les autres ne peuvent pas l'avoir. Les idées inutilisées, au contraire, sont inutiles pour ceux qui les ont. Un autre peut aussi les avoir, en arrivant aux mêmes réflexions, qu'on les ait cadenassées à triple tour ou non. Comme le chantait autrefois <a href="http://fr.wikipedia.org/wiki/Die_Gedanken_sind_frei">Hoffmann von Fallersleben</a> :
<blockquote>« Les pensées sont libres »</blockquote>
C'est le gros inconvénient des trucs qui ne se laissent pas saisir à pleines mains. Au moins du point de vue de ceux qui aimeraient bien pouvoir les contrôler pour les faire casquer. En l'occurrence cette « liberté de la pensée » est un avantage. Car celui qui partage ses idées avec les autres, il les multiplie à l'usage de tous et par retour aussi du sien propre. Plus la régulation de cet usage est libre, plus ceux qui en profitent sont nombreux. C'est là que gît le véritable bénéfice de tels biens immatériels. Celui qui cherche cependant à verrouiller les idées comme l'I., par exemple en limitant leur diffusion, il prive la société de quelque chose. Et dans le doute il court le risque de considérer beaucoup d'humains <a href="http://www.indiskretionehrensache.de/2012/02/acta-jugend/">comme des criminels</a> et de les censurer. Alors beaucoup payent un prix élevé pour que quelques uns en profitent. C'est exactement ça que le terme veut <del>voiler</del> justifier.

[^1]: En allemand, geistiges Eigentum veut littéralement dire « propriété spirituelle ».

