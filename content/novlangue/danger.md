Title: Danger
Date: 2012-03-22 10:04:15
License: <a href="http://creativecommons.org/licenses/by-nc-sa/2.0/de/"><i class="icon-legal"></i>CC-BY-NC-SA</a>
Source: <a href="http://neusprech.org/gefaehrder/"><i class="icon-external-link"></i> Gefährder</a>
Author: Kai Biermann

Au sens étatique, une personne menaçante pour la sûreté des citoyens et de l'État, principalement un <a href="http://novlangue.sploing.fr/danger-potentiel/">potentiel</a> terroriste. D. sonne comme « menace ». Mais plus exactement on fait référence à quelqu'un contre qui on n'a aucune charge judiciaire, qu'il est impossible de traduire en justice, encore moins de condamner, et qui est innocent selon notre conception actuelle du droit. Ce que les D. sont au juste, personne ne le dit. Parfois ce sont des personnes soupçonnées hâtivement. Mais en tout cas ce sont des hommes qu'il convient de tenir à l'œil, ou autrement dit, de mettre sous surveillance.
