Title: Réfractaire au progrès
Date: 2013-04-13 15:00:15
License: <a href="http://creativecommons.org/licenses/by-nc-sa/2.0/de/"><i class="icon-legal"></i>CC-BY-NC-SA</a>
Source: <a href="http://neusprech.org/fortschrittsverweigerer"><i class="icon-external-link"></i> Fortschrittsverweigerer</a>
Author: Stefan Kruppa

Aussi appelé réfractaire à la [technique](http://www.zeit.de/2010/49/Infrastruktur-Buergerprotest) ou à la [modernité](http://www.boulevard-baden.de/ueberregionales/baden-wuerttemberg/2010/11/24/landtag-streitet-uber-verstandnis-der-grunen-von-modernisierung-284780). Désigne les opposants aux plans qui doivent être soutenus pour <del>améliorer la société</del> créer de la croissance. Insulter de r. au p. est essayer de disqualifier toute critique envers ces nouveautés, car cela fait appel à la croyance commune que le neuf est toujours meilleur que l'ancien. Il faut être <del>bouché</del> idiot pour critiquer le progrès. Peu importe si celui-ci convient. Dès lors, la pertinence des contre-arguments importe peu et le débat n'est pas nécessaire. Se demander si l'introduction d'une nouvelle technique est effectuée de manière responsable confine à l'ignorance ou signale un esprit borné. C'est une sorte de prise de judo de la novlangue qui retourne l'énergie de l'opposant contre lui-même. D'où son utilisation fréquente. Le plus souvent elle justifie des projets risqués et/ou chers qui touchent à l'énergie nucléaire ou à la génétique.


