Title: Retrait
Date: 2012-03-22 10:04:15
License: <a href="http://creativecommons.org/licenses/by-nc-sa/2.0/de/"><i class="icon-legal"></i>CC-BY-NC-SA</a>
Source: <a href="http://neusprech.org/ausstieg"><i class="icon-external-link"></i> Ausstieg</a>
Author: Martin Haase

Pour le dire gentiment, la politique est comme la vie au ralenti; une décision qui nous prend quelques secondes peut sans soucis durer des décennies. En fait se retirer est un verbe terminatif, ce qui veut dire qu'il désigne un moment temporel unique, ultime (comme la préposition « jusqu'à » en français). Par conséquent si quelqu'un quitte le bus à 15h31, après il est dehors. Quand il dit qu'il voulait simplement se retirer, il fait précisément référence à ce court moment. Par chance pour les politiques allemands, cette manière terminative de parler, cette aspectualité, se perd quand le verbe est substantivé et devient alors le mot R. Duquel on peut beaucoup parler sans devoir dire quand l'ensemble suit finalement son cours. Se retirer va vite, mais un R., ça peut se prolonger. C'est aussi pour ça que le style nominal est tellement aimé dans les discours politiques et que les verbes ne le sont pas tant que ça. Parce qu'en politique on préfère discuter longtemps des faits de les traiter rapidement. Ça rapporte plus <del>de voix</del> <del>de manchettes</del> d'attention.

