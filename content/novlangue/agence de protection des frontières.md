Title: Agence européenne de protection des frontières extérieures
Date: 2013-04-07 10:00:15
License: <a href="http://creativecommons.org/licenses/by-nc-sa/2.0/de/"><i class="icon-legal"></i>CC-BY-NC-SA</a>
Source: <a href="http://neusprech.org/grenzschutzagentur"><i class="icon-external-link"></i> Grenzschutzagentur</a>
Author: Kai Biermann

La langue des politiques ne cherche [pas toujours](http://www.afrique-europe-interact.net/index.php?article_id=422&clang=2) l'emphase. Parfois il s'agit plutôt de banaliser et dédramatiser. [Comme pour l'A.](http://www.immigration.interieur.gouv.fr/Europe-International/La-circulation-transfrontiere/FRONTEX) qui utilise pour ce faire le terme agence. Lexicalement elle ressemble alors à un [simple bureau](http://fr.wiktionary.org/wiki/agence), qui pourrait simplement vendre de la protection des frontières, ou représenter les protecteurs des frontières, ou protéger une frontière de quelqu'un. En teneur, il s'agit pourtant de [policiers mal dégrossis et armés](http://fr.wikipedia.org/wiki/Agence_europ%C3%A9enne_pour_la_gestion_de_la_coop%C3%A9ration_op%C3%A9rationnelle_aux_fronti%C3%A8res_ext%C3%A9rieures) qui patrouillent les frontières européennes et chassent les hommes qui s'en approchent, quelque chose comme une police des frontières ou son haut-commandement. Car l'A. coordonne les **polices** des frontières nationales. L'acronyme Frontex est encore moins utile pour comprendre la teneur réelle de l'A. Il s'agit d'une abréviation du français «Frontières extérieures». Rien à voir avec les réfugiés disparus, les activités policières ou les tests de nouvelles techniques de surveillance.


