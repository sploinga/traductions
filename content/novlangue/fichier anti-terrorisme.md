Title: Fichier anti-terrorisme
Date: 2012-03-22 10:04:15
License: <a href="http://creativecommons.org/licenses/by-nc-sa/2.0/de/"><i class="icon-legal"></i>CC-BY-NC-SA</a>
Source: <a href="http://neusprech.org/antiterrordatei"><i class="icon-external-link"></i> Antiterrordatei</a>
Author: Kai Biermann

On n'y stocke pas les terroristes, comme le nom le laisse entendre, mais les terroristes supposés ou les personnes putativement suspectes, c'est-à-dire tous ceux qui achètent un baril d'eau oxygénée ou plus simplement veulent payer leur loyer en liquide. Les apprentis-pilotes ou les fidèles des mosquées peuvent être intégrés dans le fichier. De même que leurs appels téléphoniques avec leurs cercles amicaux, lors par exemple de l'achat d'une voiture. Parce que des données sur les « soutiens » et les « contacts » sont aussi stockées. Géré par l'<a href="http://www.bka.de/">Office Fédéral de la Lutte contre la Criminalité</a>, utilisé par tous les services et les policiers de ce pays, le F. A.-T. sert en fait à enfreindre la séparation des services. Mais ce n'est que le moindre des soucis. Car ces derniers temps c'est une collection de données rassemblées selon des critères inconnus, pour une durée incertaine et à des buts mal définis. Voir <a href="http://novlangue.sploing.fr/danger/">Danger</a>, ou plutôt <a href="http://novlangue.sploing.fr/danger-potentiel/">Danger potentiel</a>. Ça peut sembler improbable, mais selon la définition <a href="http://www.bundesverfassungsgericht.de/entscheidungen/es20080507_2bve000103.html">quelques politiques</a> doivent y figurer en bonne place. Il reste que dans la <a href="http://dipbt.bundestag.de/dip21/btd/16/029/1602950.pdf">législation</a>, sont fichés tous ceux qui :

<blockquote>« employent illégalement la violence pour faire prévaloir des intérêts politiques ou religieux extérieurs à la nation, ou soutiennent, préparent, prônent ou appellent intentionnellement par leurs activités à un tel emploi de la violence. »
</blockquote>

Mais soutenir <a href="http://fr.wikipedia.org/wiki/Guerre_d%27Irak">les guerres d'agression</a>, ça compte pour du beurre.

<sub>Ceci est une traduction de <a href="http://neusprech.org/antiterrordatei/">l'article</a> de Neusprech.org.</sub>

