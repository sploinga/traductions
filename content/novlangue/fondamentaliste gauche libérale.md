Title: Fondamentaliste de la gauche libérale
Date: 2012-03-22 10:04:15
License: <a href="http://creativecommons.org/licenses/by-nc-sa/2.0/de/"><i class="icon-legal"></i>CC-BY-NC-SA</a>
Source: <a href="http://neusprech.org/fundamentalisten-linksliberale/"><i class="icon-external-link"></i> Fundamentalisten, linksliberale</a>
Author: Martin Haase

Ça sonne tout à fait faux et ça doit, car c'est un outrage volontaire, qui joue avec les préjugés avec la même virtuosité que <a href="http://politiquedunetz.sploing.fr/2012/03/plutot-mourir-que-detre-dans-bild/">Bild</a>. Vous ne me croyez pas ? Bon, à quoi vous pensez en disant fondamentaliste ? Exactement, à Al-Qaïda. Et en disant gauchiste ? Exactement, à des voitures en feu et à des pavés volants. Maintenant en disant libéral, et même libéral de gauche ? Au FDP ? (cf le Modem en France) Vous blaguez. Vous ne pensez pas plutôt aux défenseurs du mariage homo ou aux objecteurs de conscience ou à d'autres types de « faux derches » ? C'est en tout cas ce qu'escomptait le ministre de l'intérieur Friedrich (cf Claude Guéant en France) quand il a qualifié dans Bild (sic!) les F. de la G. L. de 

<blockquote>« détracteurs des lois anti-terrorisme »</blockquote>

C'est du reste un oxymore, les fondamentalistes sont rarement libéraux. Vous devriez pourtant considérer <a href="http://novlangue.sploing.fr/danger/">dangereux</a> tous ceux qui en appellent à la constitution et avancent que trop de surveillance, même avec les meilleurs intentions du monde, n'est pas une bonne chose.

En l'occurrence la constitution est une collection de droits défensifs. Elle cherche à protéger l'individu de l'État. Car la constitution ne se méfie pas de « son propre État de droit », comme Friedrich le reproche à ses opposants, elle se méfie de n'importe quel État. À cause d'une longue et douloureuse histoire. Par conséquent c'est une étonnante torsion de la part du ministre d'affirmer que l'État qui voudrait se borner à sa sphère de pouvoir mettrait en danger « les corps et les vies d'innocents ». Puisque restreindre l'emprise de l'État, c'est garantir ses libertés à chacun et donc in fine son corps et sa vie. Car personne d'autre dans l'histoire n'a opprimé, supplicié et tué avec autant de succès et aussi peu d'états d'âme que les appareils d'États tout-puissants et leurs fonctionnaires euphoriquement inattaquables. 

Friedrich a donc recours à une connotation négative, ce que l'on appelle en langue savante une péjoration. On pourrait aussi dire qu'il pratique l'art de la propagande.
