Title: Cybercriminalité
Date: 2013-04-07 10:00:15
License: <a href="http://creativecommons.org/licenses/by-nc-sa/2.0/de/"><i class="icon-legal"></i>CC-BY-NC-SA</a>
Source: <a href="http://neusprech.org/internetkriminalität"><i class="icon-external-link"></i> Internetkriminalität</a>
Author: Kai Biermann

[Les politiques](http://www.csu-landesgruppe.de/Titel__internetkriminalitaet_nimmt_zu/TabID__6/SubTabID__7/InhaltTypID__1/InhaltID__19658/Inhalte.aspx) ou [la sécurité intérieure](http://www.spiegel.de/netzwelt/web/cybercrime-2011-bka-legt-bericht-zu-internetkriminalitaet-vor-a-856521.html) ne parlent pas ou peu de criminalité épistolaire, de criminalité [pneumatique](http://fr.wiktionary.org/wiki/pneumatique) ou de criminalité automobile. Pour ne prendre que quelques exemples. Il existe des centaines d'outils possibles pour commettre crimes et délit. [Internet n'est que l'un d'entre eux](http://fr.wikipedia.org/wiki/Cybercrime). Pourquoi alors est-ce que les politiques et les policiers parlent de C. ? Il faut croire que leur expression véhicule quelques préjugés. Parce qu'Internet est aussi neutre vis-à-vis de la criminalité que les lettres, les tubes pneumatiques, les voitures ou les diligences. Ce sont les hommes qui enfreignent le droit d'auteur, harcèlent ou fraudent. Pas Internet. 


