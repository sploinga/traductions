Title: Experts
Date: 2013-04-08 10:00:15
License: <a href="http://creativecommons.org/licenses/by-nc-sa/2.0/de/"><i class="icon-legal"></i>CC-BY-NC-SA</a>
Source: <a href="http://neusprech.org/experte"><i class="icon-external-link"></i> Experte</a>
Author: Kai Biermann

Il y a des mots qui parsèment nos phrases bien qu'ils n'aient aucune signification profonde. Ils doivent seulement nous aider à cicatriser les failles pour nous donner un sentiment de sécurité. En linguistique, on les appelle des passe-partouts, justement parce qu'ils peuvent être placés n'importe où. E. est un tel bouche-trou. Il y a une époque où cela désignait ceux qui excellaient particulièrement dans un domaine. Aujourd'hui, les médias utilisent ce mot pour tous ceux qui n'ont pas de métier clairement référencé ou dont le métier est tellement compliqué que la dénomination ne tient pas dans le bas de l'écran.  Ainsi on parle d'[e. Internet, d'e. en terrorisme, d'e. économiques](http://www.youtube.com/watch?v=EEG1_iBcfRI), parfois même en précisant avec emphase «spécialiste». Ça fait sérieux et sert à faire oublier que l'intéressé n'en sait pas plus sur la situation que l'auditeur ou le journaliste. Car vu que, au doigt mouillé, les médias envoient au casse-pipe tous les jours trois-cent experts rien qu'en Allemagne, ils en manquent un peu. C'est pourquoi le terme est surtout de la novlangue médiatique et ne signifie plus réellement «expert», mais «quelqu'un qui était d'accord pour discuter avec nous de ces âneries». Ou, comme l'expert des médias Stefan Niggemeier écrivait récemment dans la [Frankfurter Allgemeinen Sonntagszeitung](http://www.faz.net/aktuell/feuilleton/medien/2.1756/fernsehkommentare-zum-terror-wer-solche-experten-kennt-braucht-keine-laien-11109925.html) : 

> Celui qui connaît de tels experts n'a plus besoin de béotiens.


