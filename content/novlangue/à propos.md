Title: À propos de la catégorie Novlangue
Date: 2012-03-22 10:00:15
Summary: Traduction des articles de très bonne facture de Neusprech.org, et ajout de vos propres définitions si elles sont aussi réussies.

<a href="http://neusprech.org">Neusprech.org</a>, c'est

*	Martin Haase, Professor de Linguistique à l'université Otto-Friedrich de Bamberg, membre du Chaos Computer Clubs et pirate patenté.
*	Kai Biermann, Psychologue, Journaliste, Auteur et Rédacteur à Ressort Digital sur ZEIT ONLINE.

Voilà ce qu'ils déclarent dans leur page <a href="http://neusprech.org/eine-seite/">À propos de nous</a>:

>>« Celui qui veut délibérément dissimuler quelque chose aux autres ou à lui-même, même si ce quelque chose, même sans se rendre bien compte lui-même de ce qu'est ce quelque chose : la langue le révèle. »

><a href="http://fr.wikipedia.org/wiki/Victor_Klemperer">Victor Klemperer</a> a écrit cette phrase en tant que philologue dans le calepin qui lui servait à sonder les abîmes de la LTI, ou <a href="http://fr.wikipedia.org/wiki/Lingua_Tertii_Imperii">Lingua Tertii Imperii</a>, ou langue du national-socialisme, afin de mettre les intentions de cette langue à nu. George Orwell a propagé dans son livre 1984 l'idée qu'à travers une novlangue, la création intentionnelle de nouveaux mots, la pensée elle-même pouvait être influencée. Et Joseph Putlitzer revendiquait :

>>« Il n'y a pas de crime, d'astuce, de faux-fuyant, de fumisterie ou de vice qui ne vive du sceau du secret. Remonte cette clandestinité de la cave à la lumière du soleil, décris-la , montre son ridicule à la face du monde. Et tôt ou tard l'opinion publique la balayera comme une malpropre. Rendre public quelque chose ne suffit sans doute pas, mais c'est l'unique ressource qui nous soit essentielle. Sans elle toutes les autres flanchent. »

>Les prémisses de ces hommes valent aujourd'hui encore, d'autant plus à notre époque des médias de masse et des relations publiques en politique. Ou comme le déclare Howard Dean dans son introduction à *Don't think of an elephant* de George Lakoff :

>>Language matters, Sprache ist wichtig, La langue, ça compte.

Pourquoi donc ne pas essayer de chercher quels sont les mots que les politiques utilisent pour nous embrouiller, pourquoi ne pas mettre noir sur blanc ce que signifient tous ces détours linguistiques et créations langagières que nous utilisons si naturellement ? N'hésitez pas à [participer](/pages/participer.html) !

Contact à <trads@sploing.fr>.

Bonne visite !
