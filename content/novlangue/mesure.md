Title: Mesure
Date: 2012-03-22 10:04:15
License: <a href="http://creativecommons.org/licenses/by-nc-sa/2.0/de/"><i class="icon-legal"></i>CC-BY-NC-SA</a>
Source: <a href="http://neusprech.org/massnahme"><i class="icon-external-link"></i> Maßnahme</a>
Author: Martin Haase

Terme générique (comme trucs, boum ou euh dans la langue courante) pour toute sorte d'activismes politiques. D'où les tentatives de feindre une action ciblée, par exemple avec l'annonce de projets législatifs, de M. contre la pédopornographie. Ou aussi de s'empresser de camoufler ses actions, comme avec les policiers chez qui il s'agit plutôt de perquisitions, d'écoutes téléphoniques ou détentions. 
