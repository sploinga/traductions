Title: Quels textes ? 

Les textes rassemblés ici sont des traductions que [j](/traducteur/xavier-gillard.html)'effectue pour avant tout mon plaisir. Ils peuvent porter sur tout ce qui m'intéresse, ou intéresserait les [éventuels contributeurs](/pages/participer.html). Ils ont tous en commun d'avoir été traduits avec l'accord de l'auteur ou d'être sous une licence qui le permettait.

D'avance, je préviens que mon avis personnel n’est pas nécessairement celui que l’article traduit défend ! Par ailleurs, comme une traduction n'est pas un simple copié-collé, mon avis entre en ligne de compte lorsque je reformule ce qui ne paraît pas toujours clair dans l’original. Si je mutile le texte, [signalez-le](/pages/participer.html) moi :).

Historiquement, j'ai commencé à tenir un blog de traduction à <http://politiquedunetz.sploing.fr> quand j'étais en Érasmus en Allemagne pour m'entraîner en allemand et en anglais, et j'ai gardé l'habitude. Le blog précédent a été abandonné parce que je n'ai pas réussi à amener autant de gens que je voulais à [participer](/pages/participer.html), et parce que je trouvais à la longue le thème du blog trop restreint. Je suis cependant fier d'avoir pu traduire [un livre entier](http://reformedroitauteur.sploing.fr/) grâce à lui.

Le rythme de publication est relativement aléatoire, dépendant de mon humeur et de l'état de mon calendrier. Le blog en lui-même est statique, ce qui me permet de ne pas me soucier de la sécurité de l'hébergement, et donc ne n'oblige pas à revenir régulièrement voir si rien n'a bougé.

Bonne lecture !
