Title: À traduire

Parmi les sources de textes dont la traduction m'intéresserait et que je garde en marque-page se trouvent :

* 	Les articles de détricotage de la novlangue allemande de <http://neusprech.org>, en particulier
	*	<http://neusprech.org/unbemanntes-luftfahrtsystem/>
	*	<http://neusprech.org/schutzluecke/>
*	Les analyses de fond de Falkvinge à <http://falkvinge.net>
*	Les explications philosophico-techniques de Steve Klabnik à <http://words.steveklabnik.com/>
*  	…
