Site disponible à <http://traductions.sploing.fr>. Tout le site est sous CC-BY-SA sauf précisé autrement.

Pour plus d'infos, aller voir:

*	<http://traductions.sploing.fr/pages/quels-textes.html>
*	<http://traductions.sploing.fr/pages/licence.html>
*	<http://traductions.sploing.fr/pages/participer.html>
